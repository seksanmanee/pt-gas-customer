import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  // {
  //   path: 'cart',
  //   loadChildren: () => import('./cart/cart.module').then( m => m.CartPageModule)
  // },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule)
  },
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  // },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  // {
  //   path: 'orders',
  //   loadChildren: () => import('./orders/orders.module').then( m => m.OrdersPageModule)
  // },
  {
    path: 'product',
    loadChildren: () => import('./product/product.module').then( m => m.ProductPageModule)
  },
  // {
  //   path: 'profile',
  //   loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  // },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'orderdetail',
    loadChildren: () => import('./orderdetail/orderdetail.module').then( m => m.OrderdetailPageModule)
  },
  {
    path: 'pdpa',
    loadChildren: () => import('./pdpa/pdpa.module').then( m => m.PdpaPageModule)
  },
  {
    path: 'cartconfirm',
    loadChildren: () => import('./cartconfirm/cartconfirm.module').then( m => m.CartconfirmPageModule)
  },
  {
    path: 'order-direction-map',
    loadChildren: () => import('./order-direction-map/order-direction-map.module').then( m => m.OrderDirectionMapPageModule)
  },
  {
    path: 'address',
    loadChildren: () => import('./address/address.module').then( m => m.AddressPageModule)
  },
  {
    path: 'address-add',
    loadChildren: () => import('./address-add/address-add.module').then( m => m.AddressAddPageModule)
  },
  {
    path: 'registertype',
    loadChildren: () => import('./registertype/registertype.module').then( m => m.RegistertypePageModule)
  },
  // {
  //   path: 'selectaddress',
  //   loadChildren: () => import('./selectaddress/selectaddress.module').then( m => m.SelectaddressPageModule)
  // },
  {
    path: 'selectpromotion',
    loadChildren: () => import('./selectpromotion/selectpromotion.module').then( m => m.SelectpromotionPageModule)
  },
  {
    path: 'regrster-max-card',
    loadChildren: () => import('./regrster-max-card/regrster-max-card.module').then( m => m.RegrsterMaxCardPageModule)
  },  {
    path: 'promotion-detail',
    loadChildren: () => import('./promotion-detail/promotion-detail.module').then( m => m.PromotionDetailPageModule)
  }



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
