import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationExtras } from '@angular/router';
import { MenuController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  userData: any;
  data: any;
  dataGrpId: any;
  bannersImage: any;
  btnMenus: any;
  btnArrMenu: any;
  listProductAll: any;
  dataProd: any;
  currentGroup: string;
  sCore: any;

  slideOpt = {
    initialSlide: 1,
    speed: 500,
    loop: true,
    autoplay: true,
  };

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public apiservice: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public storageService: StorageService
  ) {
    this.menuCtrl.enable(true);

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        this.storageService.remove('dataProd');
        this.storageService
          .getObject('logined')
          .then((result) => {
            if (result != null) {
              console.log(result);
              this.userData = result;
              this.GetScore();
            }
          })
          .catch((e) => {
            console.log('error: ', e);
          });
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
      }
    });
  }

  ngOnInit() {
    //currentGroup = 1;

    this.GetMaterialGroup();
    // this.getPromotion();
    this.gotoGrpById(2);
    this.bannersImage = [
      {
        url: '/profile',
        title: 'open Google',
        image: 'slide_4.jpg',
      },
      {
        url: '/home',
        title: 'open Facebook',
        image: 'slide_2.jpg',
      },
      {
        url: '/home',
        title: 'open Facebook',
        image: 'slide_1.jpg',
      },
      {
        url: '/home',
        title: 'open Facebook',
        image: 'slide_3.jpg',
      },
    ];

    this.dataGrpId = [
      // {
      //   mat_group_desc: 'Household LPG',
      //   mat_group_desc_thai: 'สินค้าแก๊สถัง',
      //   mat_code: '2009998',
      //   mat_desc: '15 KG ถังแก๊ส',
      //   price_inc_vat: 318.0,
      //   price_delivery: 15.0,
      //   dep_price: 1800.0,
      //   service_price: 800.0,
      //   dep_mat_sap: '500000330',
      //   service_mat_sap: '500000336',
      //   image_detail: [
      //     {
      //       mat_image_name: '15KG.png',
      //       uri: 'https://devcentralportalstorage.blob.core.windows.net/material-image/2009998/15KG.png',
      //     },
      //   ],
      // },
      // {
      //   mat_group_desc: 'Household LPG',
      //   mat_group_desc_thai: 'สินค้าแก๊สถัง',
      //   mat_code: '2009999',
      //   mat_desc: '4 KG แก๊สถัง',
      //   price_inc_vat: 318.0,
      //   price_delivery: 15.0,
      //   dep_price: 900.0,
      //   service_price: 300.0,
      //   dep_mat_sap: '500000329',
      //   service_mat_sap: '500000335',
      //   image_detail: [
      //     {
      //       mat_image_name: '4KG.png',
      //       uri: 'https://devcentralportalstorage.blob.core.windows.net/material-image/2009999/4KG.png',
      //     },
      //   ],
      // },
      // {
      //   mat_group_desc: 'Household LPG',
      //   mat_group_desc_thai: 'สินค้าแก๊สถัง',
      //   mat_code: '2009998',
      //   mat_desc: '15 KG ถังแก๊ส',
      //   price_inc_vat: 318.0,
      //   price_delivery: 15.0,
      //   dep_price: 1800.0,
      //   service_price: 800.0,
      //   dep_mat_sap: '500000330',
      //   service_mat_sap: '500000336',
      //   image_detail: [
      //     {
      //       mat_image_name: '15KG.png',
      //       uri: 'https://devcentralportalstorage.blob.core.windows.net/material-image/2009998/15KG.png',
      //     },
      //   ],
      // },
      // {
      //   mat_group_desc: 'Household LPG',
      //   mat_group_desc_thai: 'สินค้าแก๊สถัง',
      //   mat_code: '2009999',
      //   mat_desc: '4 KG แก๊สถัง',
      //   price_inc_vat: 318.0,
      //   price_delivery: 15.0,
      //   dep_price: 900.0,
      //   service_price: 300.0,
      //   dep_mat_sap: '500000329',
      //   service_mat_sap: '500000335',
      //   image_detail: [
      //     {
      //       mat_image_name: '4KG.png',
      //       uri: 'https://devcentralportalstorage.blob.core.windows.net/material-image/2009999/4KG.png',
      //     },
      //   ],
      // },
    ];

    setTimeout(() => {
      this.getPromotion();
    }, 500);
  }

  async getPromotion() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    try {
      //await loading.present();
      this.apiservice.getPromotion(this.userData).subscribe(
        (response) => {
          this.bannersImage = JSON.parse(response);
          console.log(this.bannersImage);
          loading.dismiss();
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    } catch (error) {
      console.log('Try Error', error);
      loading.dismiss();
    }
  }

  async GetMaterialGroup() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiservice.GetMaterialGroup().subscribe(
      (response) => {
        // console.log(response);
        this.btnMenus = response.data;
        let num = 1;
        let group = [];
        const arr = [];

        this.btnMenus.forEach((item) => {

          //if ((item.Mat_group_id !== 3) && (item.Mat_group_id !== 4) && (item.Mat_group_id !== 5))
          if ((item.Mat_group_id !== 3) && (item.Mat_group_id !== 4)) //accept group 3,4
          {  
              if (num % 2 !== 0) {
                group.push({ id: item.Mat_group_id, name: item.Mat_group_desc_thai });
              } else {
                group.push({ id: item.Mat_group_id, name: item.Mat_group_desc_thai });
                arr.push(group);
                group = [];
              }
              num++;
          }

        });

        //Add array for fulfill array lenght
        if(group.length = 1)
        {
          group.push({ id: 0, name: '' });
          arr.push(group);
        }

        this.btnArrMenu = arr;
        // console.log(arr);
        loading.dismiss();
        // var pp = 0;
        // this.btnMenus.forEach(function(value) {
        //   if (pp === 0) {
        //     pp = value.mat_group_id;
        //     this.storageService.setObject('grpSelect', {
        //       grpId: pp
        //     });
        //     this.GetMaterialGroupById(pp);
        //   }
        // });
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  seeDetail(id: string, type: string, video: string) {
    if (type === '1') {
      this.router.navigateByUrl('/articledetail/' + id);
    } else if (type === '2') {
      const encode = video.replace(/\//g, '%2F');
      this.nav.navigateForward('/videodetail/' + id + '/' + encode);
    }
  }

  async getProduct(matCode) {
    this.storageService
      .getObject('logined')
      .then(async (result) => {
        this.userData = result;
        if (this.userData !== null) {
          this.storageService
            .getObject('listProd')
            .then((result) => {
              if (result != null) {
                this.dataProd = result.filter((d) => d.Mat_code === matCode);
                this.setProductToLocal();
              }
            })
            .catch((e) => {
              console.log('error: ', e);
            });
        } else {
          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบก่อน.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  async setProductToLocal() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.storageService.setObject('dataProd', this.dataProd);
    loading.dismiss();
    this.router.navigateByUrl('product');
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    setTimeout(() => {
      this.data = {
        heading: 'Normal text',
        para1: 'Lorem ipsum dolor sit amet, consectetur',
        para2: 'adipiscing elit.',
      };
    }, 500);

    this.storageService.remove('dataProd');
  }

  gotoPromotionDetails(item) {
    console.log(item.Discount_id);
    // this.router.navigate(['/promotion-detail', { item: item.Discount_id }]);

    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(item),
      },
    };
    this.router.navigate(['promotion-detail'], navigationExtras);
  }

  gotoGrpById(grpId) {
    this.currentGroup = grpId;
    this.GetMaterialGroupById(grpId);
  }

  async GetMaterialGroupById(grpId) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiservice.GetMaterialGroupByID(grpId).subscribe(
      (response) => {
        this.dataGrpId = JSON.parse(response);
        console.log(this.dataGrpId);
        this.storageService.setObject('listProd', this.dataGrpId.data);
        loading.dismiss();
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
