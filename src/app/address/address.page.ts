import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {
  userData: any;
  sCore: any;
  addressData: any;
  activeAddress: any;
  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public alertCtrl: AlertController,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('activeAddress')
      .then((result) => {
        if (result != null) {
          this.activeAddress = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator
    //     this.loadAddress();
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadAddress();
  }

  async loadAddress() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // await loading.present();

    setTimeout(() => {
      // this.apiservice.GetCustomerAddress(this.userData).subscribe(
      //   (res) => {
      //     const data = JSON.parse(res);
      //     if (data.response.responseCode === '0000') {
      //       this.addressData = data.data;
      //       console.log(this.addressData);
      //       let tmpData = data.data;
      //       data.data.forEach((value, key) => {
      //         if (this.activeAddress?.Branch_code === value?.Branch_code) {
      //           tmpData[key]['active'] = true;
      //         } else {
      //           tmpData[key]['active'] = false;
      //         }
      //       });

      //       this.addressData = tmpData;
      //       // console.log(this.addressData);
      //       this.storageService.setObject('listAddress', this.addressData);
      //     } else {
      //       this.presentToast(data.response.responseMsg);
      //     }
      //     loading.dismiss();
      //   },
      //   (error) => {
      //     loading.dismiss();
      //     console.log(error);
      //   }
      // );

      this.apiservice
        .GetCustomerAddress(this.userData)
        .then(async (res: string) => {
          const data = JSON.parse(res);
          if (data.response.responseCode === '0000') {
            this.addressData = data.data;
            console.log(this.addressData);
            let tmpData = data.data;
            data.data.forEach((value, key) => {
              if (this.activeAddress?.Branch_code === value?.Branch_code) {
                tmpData[key]['active'] = true;
              } else {
                tmpData[key]['active'] = false;
              }
            });

            this.addressData = tmpData;
            // console.log(this.addressData);
            this.storageService.setObject('listAddress', this.addressData);
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        })
        .catch(async (error) => {
          loading.dismiss();
          console.log(error);

          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบก่อน.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        });
    }, 500);
  }

  async remove(branchCode) {
    const alert = await this.alertCtrl.create({
      header: 'ยืนยัน',
      message: 'คุณแน่ใจว่าต้องการลบหรือไม่?',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ไม่',
          handler: () => {},
        },
        {
          text: 'ใช่',
          handler: () => {
            this.asyncremoveAddress(branchCode);
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  async asyncremoveAddress(branchCode) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    await loading.present();
    this.apiservice.DeleteCustomerAddress(this.userData, branchCode).subscribe(
      async (response) => {
        if (response === undefined) {
          const alert = await this.alertCtrl.create({
            header: 'แจ้งเตือน',
            message: 'ไม่สามารถลบที่อยู่ได้',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {},
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
          loading.dismiss();
        } else {
          loading.dismiss();
          this.presentToast('ลบที่อยู่เรียบร้อยแล้ว');
          this.loadAddress();
        }
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async presentAlertConfirm(title: string, txtMessage: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: txtMessage,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ไม่',
          handler: () => {},
        },
        {
          text: 'ใช่',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  gotoAddressAdd() {
    this.router.navigateByUrl('address-add');
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  selectAddress(data) {
    this.activeAddress = data;
    this.loadAddress();
    this.storageService.setObject('activeAddress', data);
    setTimeout(() => {
      this.router.navigateByUrl('/tabs/cart');
    }, 100);
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
