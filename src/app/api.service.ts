import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
export class Users {
  token: string;
  username: string;
  userRole: string;
  singlerole: string;
  account_id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  password: string;
  loginError: string;
  contentType: string;
  serializerSettings: string;
  value: string;
}
export class DataProd {
  response: {
    responseCode: string;
    responseMsg: string;
  };
  data: {
    Mat_group_desc: string;
    Mat_group_desc_thai: string;
    Mat_code: string;
    Mat_desc: string;
    Price_inc_vat: number;
    Price_delivery: number;
    Dep_price: number;
    Service_price: number;
    Dep_mat_sap: string;
    Service_mat_sap: null;
    image_detail: {
      Mat_image_name: string;
      uri: string;
    };
  };
}
export class DataPromotion {
  'diffgr:diffgram': any;
}
export class DataPromotionaa {
  PRODUCT_CODE: string;
  PRODUCT_NAME: string;
  PRODUCT_DESC: string;
  PRODUCT_IMG_URL: string;
  PRODUCT_THUMB_URL: string;
  PRODUCT_POINT: string;
  PRODUCT_AMOUNT: string;
  CATALOG_CODE: string;
  HIGHLIGHT_STATUS: string;
  UPDATE_DATE: string;
  REDEEM_CATEGORY_TYPE: string;
  SEQUENCE: string;
  PRODUCT_CONDITION: string;
  PRODUCT_NEW_STATUS: string;
  CREATE_DATE: string;
  READMORE_STATUS: string;
  SPECIAL_STATUS: string;
  START_DATE: string;
  END_DATE: string;
  ABOUT_TWITTER: string;
  ABOUT_FACEBOOK: string;
}
export class DataMenu {
  response: {
    responseCode: string;
    responseMsg: string;
  };
  data: {
    Created_date: string;
    Created_user: string;
    Edited_date: string;
    Edited_user: string;
    Mat_group_desc: string;
    Mat_group_desc_thai: string;
    Mat_group_id: number;
    Mat_group_sap: string;
    Mat_group_status: string;
    create_user_name: string;
    edit_user_name: string;
  };
}

export class orderList {
  response: {
    responseCode: string;
    responseMsg: string;
  };
  data: {
    Order_no: string;
    Order_date: string;
    Order_time: string;
    Cus_id: string;
    customer_Name: string;
    Max_id: string;
    Phone: string;
    Email: string;
    Ship_to_desc: string;
    Latitude: number;
    Longitude: number;
    Order_qty: number;
    Total_amount: number;
    Total_discount: number;
    Status_code: string;
    Status_desc: string;
  };
}
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  httpHeader = {
    headers: new HttpHeaders({ Accept: 'application/json', 'Content-type': 'application/json; charset=utf-8' }),
  };

  //apiUrl = 'https://asv-centralportalgateway-dev.azurewebsites.net/api/mobilephase-I/';
  //apiUrlIII = 'https://asv-centralportalgateway-dev.azurewebsites.net/api/gasphase-III/';

  apiUrl = 'https://asv-centralportalgateway-prod.azurewebsites.net/api/mobilephase-I/';
  apiUrlIII = 'https://asv-centralportalgateway-prod.azurewebsites.net/api/gasphase-III/';

  constructor(private http: HttpClient) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  UsersLogin(users): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'Account/LoginCustomer', users, this.httpHeader).pipe(
      tap((_) => console.log('users fetched!')),
      catchError(this.handleError<Users>('User Login.'))
    );
  }

  addUsers(users: Users) {
    return this.http.post<any>(this.apiUrl + 'Account/Register', users, this.httpHeader).toPromise();
  }

  saveOrder(userData, orderData) {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + userData.token,
      }),
    };
    return this.http.post<any>(this.apiUrl + `OrderingOnline/PostOrderingOnline`, orderData, this.httpHeader).toPromise();
  }
  ///api/gasphase-III/Discount/GetAllPromotion

  getPromotion(userData) {
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // const token = localStorage.getItem('token');
    // this.httpHeader = {
    //   headers: new HttpHeaders({
    //     Accept: 'application/json',
    //     'Content-type': 'application/json; charset=utf-8',
    //     Authorization: 'Bearer ' + userData.token,
    //   }),
    // };
    return this.http.get<any>(this.apiUrlIII + 'Discount/GetPromotionForUse', this.httpHeader).pipe(
      tap((Articles) => console.log('articles fetched!')),
      catchError(this.handleError<any>('Get articles'))
    );
  }

  GetPromotionByMaterial(userData, Mat_code) {
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // const token = localStorage.getItem('token');
    // this.httpHeader = {
    //   headers: new HttpHeaders({
    //     Accept: 'application/json',
    //     'Content-type': 'application/json; charset=utf-8',
    //     Authorization: 'Bearer ' + userData.token,
    //   }),
    // };
    return this.http.get<any>(this.apiUrlIII + 'Discount/GetPromotionByMaterial/' + Mat_code, this.httpHeader).pipe(
      tap((Articles) => console.log('articles fetched!')),
      catchError(this.handleError<any>('Get articles'))
    );
  }

  GetPromotionByID(discountId) {
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // const token = localStorage.getItem('token');
    // this.httpHeader = {
    //   headers: new HttpHeaders({
    //     Accept: 'application/json',
    //     'Content-type': 'application/json; charset=utf-8',
    //     Authorization: 'Bearer ' + userData.token,
    //   }),
    // };
    return this.http.get<any>(this.apiUrlIII + `Discount/GetPromotionByID/` + discountId, this.httpHeader).toPromise();
  }

  GetPrefixs(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'MasterData/GetPrefixs').pipe(
      tap((Articles) => console.log('GetPrefixs fetched!')),
      catchError(this.handleError<any>('GetPrefixs'))
    );
  }

  GetMaterialGroup(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'MaterialGroup/GetMaterialGroup').pipe(
      tap((Articles) => console.log('articles fetched!')),
      catchError(this.handleError<any>('Get material'))
    );
  }

  getProvince(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'MasterData/GetProvinces', this.httpHeader).pipe(
      tap((_) => console.log('Username fetched!')),
      catchError(this.handleError<any>('Username Exits.'))
    );
  }

  getDistrict(provinceId): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'MasterData/GetDistricts/' + provinceId, this.httpHeader).pipe(
      tap((_) => console.log('Username fetched!')),
      catchError(this.handleError<any>('Username Exits.'))
    );
  }

  getSubDistrict(districtId): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'MasterData/GetSubDistricts/' + districtId, this.httpHeader).pipe(
      tap((_) => console.log('Username fetched!')),
      catchError(this.handleError<any>('Username Exits.'))
    );
  }

  GetMaterialGroupByID(grpId): Observable<any> {
    return this.http.get<DataProd>(this.apiUrl + 'MaterialGroup/GetMaterialGroupByID/' + grpId, this.httpHeader).pipe(
      tap((_) => console.log('Username fetched!')),
      catchError(this.handleError<Users>('Username Exits.'))
    );
  }

  checkemailexits(user): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'chkemail/format/json', user, this.httpHeader).pipe(
      tap((_) => console.log('Email fetched!')),
      catchError(this.handleError<Users>('Email Exits.'))
    );
  }

  checkusernameexits(user): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'chkuser/format/json', user, this.httpHeader).pipe(
      tap((_) => console.log('Username fetched!')),
      catchError(this.handleError<Users>('Username Exits.'))
    );
  }

  ResetPassword(email): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'Account/ResetPassword/' + email).pipe(
      tap((Articles) => console.log('articles fetched!')),
      catchError(this.handleError<any>('Get material'))
    );
  }

  OrderList(userData) {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + userData.token,
      }),
    };
    return this.http.get<orderList>(this.apiUrl + `OrderingOnline/GetOrderingHistoryByCustomer/${userData?.data?.Cus_id},02`, this.httpHeader).toPromise();
  }

  GetPlantAll(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'Plant/GetPlantAll', this.httpHeader).pipe(
      tap((_) => console.log('GetPlantAll fetched!')),
      catchError(this.handleError<any>('GetPlantAll Exits.'))
    );
  }

  OrderingByOrderId(selectOrder): Observable<any> {
    return this.http.get<any>(this.apiUrl + `OrderingOnline/GetOrderingByOrderId/${selectOrder.Order_no},02`, this.httpHeader).pipe(
      tap((_) => console.log('Order List fetched!')),
      catchError(this.handleError<Users>('Order List.'))
    );
  }

  UpdateStatusOrderingOnline(userData, orderId, status): Observable<any> {
    return this.http.put<any>(this.apiUrl + `OrderingOnline/UpdateStatusOrderingOnline/${orderId},${userData?.data?.Cus_id},0,02,${status}`, {}, this.httpHeader).pipe(
      tap((_) => console.log('UpdateStatusOrderingOnline fetched!')),
      catchError(this.handleError<Users>('UpdateStatusOrderingOnline List.'))
    );
  }

  GetCustomerAddress(userData) {
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + userData.token,
      }),
    };
    return this.http.get(this.apiUrl + `Customer/GetCustomerAddress/${userData?.data?.Cus_id}`, this.httpHeader).toPromise();
  }

  PostCustomerAddress(userData, data): Observable<any> {
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + userData.token,
      }),
    };
    return this.http.post<any>(this.apiUrl + `Customer/PostCustomerAddress`, data, this.httpHeader).pipe(
      tap((_) => console.log('PostCustomerAddress List fetched!')),
      catchError(this.handleError<any>('PostCustomerAddress List.'))
    );
  }

  DeleteCustomerAddress(userData, branchCode): Observable<any> {
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + userData.token,
      }),
    };
    return this.http.delete<any>(this.apiUrl + `Customer/UpdateStatusCustomerBranch/${userData?.data?.Cus_id},${branchCode},${userData?.data?.Cus_id},02`, this.httpHeader).pipe(
      tap((_) => console.log('PostCustomerAddress List fetched!')),
      catchError(this.handleError<any>('PostCustomerAddress List.'))
    );
  }

  GetProfileFromMaxCard(formRegis, mid) {
    return this.http.get<any>(this.apiUrl + `Account/GetProfileFromMaxCard/${formRegis.regisNumber},${mid},${formRegis.registerType}`, this.httpHeader).toPromise();
  }

  GetMIDCustomer() {
    return this.http.get<any>(this.apiUrl + `MasterData/GetMIDCustomer`, this.httpHeader).toPromise();
  }

  GetMasterConfig(userData) {
    // return this.http.get<any>(this.apiUrlIII + `MasterData/GetMasterConfig/1018`, this.httpHeader).toPromise();

    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + userData.token,
      }),
    };
    return this.http.get<any>(this.apiUrlIII + `MasterConfig/GetMasterConfig/1018`, this.httpHeader).toPromise();
  }

  GetScoreMaxCard(cid, mid) {
    return this.http.get<any>(this.apiUrl + `Account/GetProfileFromMaxCard/${cid},${mid},0`, this.httpHeader).toPromise();
  }

  // GetUserProfile(userData) {
  //   this.httpHeader = {
  //     headers: new HttpHeaders({
  //       Accept: 'application/json',
  //       'Content-type': 'application/json; charset=utf-8',
  //       Authorization: 'Bearer ' + userData.token,
  //     }),
  //   };

  //   const param = {
  //     mid: 'string',
  //     tid: 'string',
  //     cardNo: userData.data.Max_id,
  //     cardPoint: 0,
  //     cardSpecialPoint: 0,
  //     cardStatus: 'string',
  //     responseCode: 'string',
  //     errorMsg: 'string',
  //     canRedeemStatus: 'string',
  //     accountType: 'string',
  //   };

  //   return this.http.post<any>(this.apiUrl + `Account/GetUserProfile`, param, this.httpHeader).toPromise();
  // }
}
