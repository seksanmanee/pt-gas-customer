import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { MenuController, NavController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { AlertController, LoadingController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  userData: any;
  product: any;
  arrArticles: any;
  prodQty = 1;
  sumProdPrice = 0;
  sumDepPrice = 0;
  sumServicePrice = 0;
  sumProdDiscount = 0;
  sumProdTotal = 0;
  showPriceDetail: boolean;
  selectPromotion: any;
  selectType: number;
  sumDelivery = 0;
  sCore: any;

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public apiservice: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public storageService: StorageService
  ) {
    this.menuCtrl.enable(true);
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
        }
      })
      .catch((e) => {
        console.log('logined error: ', e);
      });

    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });
  }

  ngOnInit() {
    this.loadProd();
  }

  ionViewWillEnter() {
    this.loadProd();
    this.showPriceDetail = false;

    this.storageService
      .getObject('selectPromotion')
      .then((result) => {
        if (result != null) {
          this.selectPromotion = result;
          this.calSumPrice();
          console.log(this.selectPromotion);
        }
      })
      .catch((e) => {
        console.log('selectPromotion error: ', e);
      });
  }

  loadProd() {
    this.storageService
      .getObject('dataProd')
      .then((result) => {
        if (result != null) {
          this.product = result[0];
          this.product.prodQty = 1;

          if (this.product.Mat_code.substring(0, 2) === '20') {
            this.product.prodType = 0;
          } else {
            this.product.prodType = 1;
            this.calSumPrice();
          }
          // console.log(this.product.Service_price);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  addQty() {
    this.prodQty += 1;
    this.product.prodQty = this.prodQty;
    this.calSumPrice();
  }

  adjustQty() {
    if (this.prodQty > 1) {
      this.prodQty -= 1;
      this.product.prodQty = this.prodQty;
      this.calSumPrice();
    }
  }

  prodQtyKeyUp(e) {
    this.product.prodQty = this.prodQty;
    this.calSumPrice();
  }

  sumPrice(selectType) {
    this.selectType = selectType;
    this.calSumPrice();
  }

  calSumPrice() {
    // console.log(this.product);

    this.sumProdPrice = this.product.Price_inc_vat * this.prodQty;
    this.sumDelivery = this.product.Price_delivery * this.prodQty;
    this.sumProdDiscount = 0;

    if (this.selectPromotion) {
      // console.log(item.promotion);
      this.selectPromotion.forEach((promo) => {
        this.sumProdDiscount += promo.Discount_value;
      });
    }

    if (this.selectType === 2) {
      this.sumProdTotal = this.product.Price_inc_vat * this.prodQty + this.sumDelivery - this.sumProdDiscount;
      this.sumDepPrice = 0;
      this.sumServicePrice = 0;
      this.product.selectType = 2;
    } else if (this.selectType === 1 || this.product.prodType === 1) {
      this.sumProdTotal = (this.product.Price_inc_vat + this.product.Deposit_price + this.product.Service_price) * this.prodQty + this.sumDelivery - this.sumProdDiscount;
      this.sumDepPrice = this.product?.Deposit_price * this.prodQty;
      this.sumServicePrice = this.product?.Service_price * this.prodQty;
      this.product.selectType = 1;
    }

    this.product.sumPriceDelivery = this.sumDelivery;
    this.product.sumProdPrice = this.sumProdPrice;
    this.product.sumServicePrice = this.sumServicePrice;
    this.product.sumProdTotal = this.sumProdTotal;
    this.product.sumDepPrice = this.sumDepPrice;
    this.product.sumProdDiscount = this.sumProdDiscount;
  }

  async addToCart() {
    if (this.sumProdTotal > 0) {
      this.storageService
        .getObject('selectPromotion')
        .then((result) => {
          this.product.promotion = result;

          // console.log(this.product);
          this.addToCartStorage();
        })
        .catch((e) => {
          console.log('error: ', e);
        });
    } else {
      this.presentAlertConfirm('แจ้งเตือน', 'กรุณาเลือกประเภท', 1);
    }
  }

  async addToCartStorage() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    let dataCart = [];
    this.storageService
      .getObject('addToCart')
      .then((result) => {
        if (result != null) {
          dataCart = result;
          dataCart.push(this.product);
          // console.log(dataCart);
          this.storageService.setObject('addToCart', dataCart);
          loading.dismiss();
        } else {
          dataCart.push(this.product);
          this.storageService.setObject('addToCart', dataCart);
          loading.dismiss();
        }

        this.storageService.remove('selectPromotion');

        setTimeout(() => {
          this.router.navigateByUrl('/tabs/cart');
        }, 500);
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  selectProm() {
    this.router.navigateByUrl('/selectpromotion', { state: { Mat_code: this.product.Mat_code } });
  }

  async presentAlertConfirm(title: string, message: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: message,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
