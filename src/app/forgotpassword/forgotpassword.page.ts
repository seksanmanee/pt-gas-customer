import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  public loginForm: FormGroup;
  public submitAttempt = false;
  public textEmail = '';
  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public apiService: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public storageService: StorageService
  ) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'), Validators.required])],
    });
  }

  ngOnInit() {}

  async forgotPassword() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log('Error!');
    } else {
      const loading = await this.loadingctrl.create({
        message: 'Please wait...',
        duration: 0,
      });
      await loading.present();
      console.log(this.loginForm.value);
      this.apiService.ResetPassword(this.textEmail).subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }
  }
  goBack() {
    this.router.navigateByUrl('/login');
  }
  goTerm() {
    this.router.navigateByUrl('/termcondition');
  }
  async presentAlertConfirm(title: string, message: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {
            if (state === 1) {
              this.router.navigateByUrl('/welcome');
            }
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }
}
