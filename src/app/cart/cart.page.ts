import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { MenuController, NavController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  public loginForm: FormGroup;
  dataTotalPrice = 0;
  dataTotalDepPrice = 0;
  dataTotalDelivery = 0;
  dataTotalDiscount = 0;
  dataTotalAmount = 0;
  userData: any;
  product: any;
  cart: any;
  arrArticles: any;
  cartOrder: any;
  today = new Date();
  promotionSelect: any;
  discountTotal = 0;
  sCore: any;

  trafficLayer: any;
  map: any;
  shopData: any;
  currentPosition: any;
  addrSelect = {
    Branch_code: '-',
    Branch_desc: '-',
    Latitude: null,
    Longtitude: null,
    Plant_code: null,
    Status: '',
    active: null,
    address_detail: '-',
    contactmail: '-',
    contactname: '-',
    contacttel: '-',
  };
  showPriceDetail: boolean;

  public submitAttempt = true;
  orderDiscounts = [];

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);
    
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        this.loadCart();
        this.showPriceDetail = false;
        this.storageService.getObject('activeAddress').then((result) => {
          if (result != null) {
            this.addrSelect = result;
            console.log(this.addrSelect);
          }
        });
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
      }
    });
    //let watch = this.geolocation.watchPosition();
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
          console.log(this.userData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
    });
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        // this.loadCart();
        this.discountTotal = 0;
        this.storageService.getObject('deliveryOrder').then((result) => {
          if (result != null) {
            this.promotionSelect = result;
            result.forEach((e) => {
              this.discountTotal += e.discount_amount;
            });
          }
        });
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
      }
    });
  }

  loadCart() {
    this.storageService
      .getObject('addToCart')
      .then((result) => {
        console.log(result);

        if (result != null) {
          const tmpData = result;
          this.dataTotalPrice = 0;
          this.dataTotalDepPrice = 0;
          this.dataTotalDelivery = 0;
          this.dataTotalDiscount = 0;
          this.dataTotalAmount = 0;
          result.forEach((item, key) => {
            this.dataTotalPrice += item.sumProdPrice;
            this.dataTotalAmount += item.sumProdTotal;
            this.dataTotalDepPrice += item.sumDepPrice;
            this.dataTotalDelivery += item.sumPriceDelivery;
            tmpData[key].item_discout = 0;
            if (item.promotion) {
              // console.log(item.promotion);
              item.promotion.forEach((promo, keyPromo) => {
                this.dataTotalDiscount += promo.Discount_value;
                tmpData[key].item_discout += promo.Discount_value;

                this.orderDiscounts.push({
                  order_no: 'auto',
                  item_no: String(key + 1),
                  discount_id: promo.Discount_id,
                  discount_value: promo.Discount_value,
                  discount_qty: 1,
                  discount_amount: promo.Discount_value,
                });
              });
            }
          });

          this.cart = tmpData;
          console.log(this.cart);
        } else {
          this.cart = undefined;
          this.dataTotalPrice = 0;
          this.dataTotalDepPrice = 0;
          this.dataTotalDelivery = 0;
          this.dataTotalDiscount = 0;
          this.dataTotalAmount = 0;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  gotoHome() {
    this.router.navigateByUrl('/tabs/home');
  }

  async presentAlertConfirm(title: string, txtMessage: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: txtMessage,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  selectAddress() {
    this.router.navigateByUrl('/address');
  }

  async removeProd(idx) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.cart = this.cart.filter((d, key) => {
      return key !== idx;
    });

    this.storageService.setObject('addToCart', this.cart);
    loading.dismiss();
    setTimeout(() => {
      this.loadCart();
    }, 1000);
  }

  async gotoConfrimOrder() {
    let sum_Crm_promo_point = 0;
    this.cart.forEach((item, key) => {
      if (item.promotion) {
        item.promotion.forEach((promo, keyPromo) => {
          sum_Crm_promo_point += promo.Crm_promo_point;
          console.log(sum_Crm_promo_point);
        });
      }
    });

    if (sum_Crm_promo_point <= this.sCore) {
      if (this.addrSelect?.Branch_code !== '-') {
        this.storageService.remove('selectPromotion');
        setTimeout(() => {
          this.router.navigateByUrl('/cartconfirm');
        }, 500);
      } else {
        const alert = await this.alertCtrl.create({
          header: 'กรุณาเลือกที่อยู่',
          message: 'กรุณาเลือกที่อยู่ในการจัดส่ง',
          cssClass: 'custom-modal',
          buttons: [
            {
              text: 'ยืนยัน',
              handler: () => {
                this.router.navigateByUrl('/address');
              },
            },
          ],
          backdropDismiss: false, // <- Here! :)
        });
        await alert.present();
      }
    } else {
      const alert = await this.alertCtrl.create({
        header: 'ขออภัย',
        message: 'แต้มของท่านมีไม่เพียงพอ กรุณาตรวจสอบการเลือกใช้ส่วนลดอีกครั้ง',
        cssClass: 'custom-modal',
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.router.navigateByUrl('/tabs/cart');
            },
          },
        ],
        backdropDismiss: false, // <- Here! :)
      });
      await alert.present();
    }
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
