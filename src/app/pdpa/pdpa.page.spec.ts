import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PdpaPage } from './pdpa.page';

describe('PdpaPage', () => {
  let component: PdpaPage;
  let fixture: ComponentFixture<PdpaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdpaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PdpaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
