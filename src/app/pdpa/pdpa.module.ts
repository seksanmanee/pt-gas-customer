import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PdpaPageRoutingModule } from './pdpa-routing.module';

import { PdpaPage } from './pdpa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdpaPageRoutingModule
  ],
  declarations: [PdpaPage]
})
export class PdpaPageModule {}
