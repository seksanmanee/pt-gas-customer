import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PdpaPage } from './pdpa.page';

const routes: Routes = [
  {
    path: '',
    component: PdpaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PdpaPageRoutingModule {}
