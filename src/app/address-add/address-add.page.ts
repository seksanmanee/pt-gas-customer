import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { GoogleBasemap } from '../class/google-basemap';
import { StorageService } from '../storage.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { async } from 'rxjs/internal/scheduler/async';
declare const google: any;
@Component({
  selector: 'app-address-add',
  templateUrl: './address-add.page.html',
  styleUrls: ['./address-add.page.scss'],
})
export class AddressAddPage implements OnInit {
  public addressForm: FormGroup;
  public submitAttempt = false;

  userData: any;
  addressData: any;
  sCore: any;
  provList: any;
  distList: any;
  subdistList: any;

  GoogleBasemap = new GoogleBasemap();
  trafficLayer: any;
  map: any;
  currentPosition: { lat: number; lng: number };
  distCodeGuid = {};
  currentMarker: any;

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    public storageService: StorageService,
    public apiService: ApiService,
    public alertCtrl: AlertController,
    public nav: NavController,
    public formBuilder: FormBuilder,
    private geolocation: Geolocation,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);

    this.storageService
      .getObject('logined')
      //.then((result) => {
      //  if (result != null) {
      //    this.userData = result;
      //    this.GetScore();
      //  } 
      //})
      .then(async (result) => {
        console.log(result);
        if (result != null) {
          this.userData = result;
          this.GetScore();
        } else {
          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบก่อน.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        }
      })
      .catch((e) => {
        console.log('logined error: ', e);
      });

    this.storageService
      .getObject('listAddress')
      .then((result) => {
        if (result != null) {
          this.addressData = result;
        }
      })
      .catch((e) => {
        console.log('listAddress error: ', e);
      });

    let watch = this.geolocation.watchPosition();

    this.addressForm = formBuilder.group({
      contact_name: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      contact_phone: ['', Validators.compose([Validators.maxLength(10), Validators.required])],
      contact_email: ['', Validators.compose([Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'), Validators.required])],
      add_no: [''],
      road: [''],
      soi: [''],
      moo: [''],
      province: [0],
      district: [0],
      subdistrict: [0],
      phone: [''],
      email: [''],
      fax: [''],
    });
  }

  ngOnInit() {
    this.getProvince();
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.map = this.GoogleBasemap.initMap('mapGoogle');
    this.trafficLayer = new google.maps.TrafficLayer();

    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        console.log('geolocation', resp.coords.latitude + ' ' + resp.coords.longitude);
        this.addCurrentMarker(resp.coords);
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });
  }

  addCurrentMarker(coords) {
    this.currentPosition = { lat: coords.latitude, lng: coords.longitude };
    const image = {
      url: '../../assets/img/pincustomer.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 67),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(20, 67),
    };

    this.currentMarker = new google.maps.Marker({
      map: this.map,
      icon: image,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: this.currentPosition,
    });

    this.currentMarker.addListener('dragend', (event) => {
      this.currentPosition = { lat: event.latLng.lat(), lng: event.latLng.lng() };
      console.log(this.currentPosition);
    });

    this.map.setCenter(this.currentMarker.getPosition());
    this.map.setZoom(11);
  }

  async presentAlertConfirm(title: string, message: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: message,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  async saveAddress() {
    console.log(this.userData);
    console.log(this.addressData);

    const branch_code = this.pad('0000', this.addressData.length, true);
    const formAddress = this.addressForm.value;
    const data = {
      cus_id: this.userData.data.Cus_id,
      branchHD: [
        {
          cus_id: this.userData.data.Cus_id,
          branch_code: branch_code,
          branch_desc: 'HQ',
          status: 'A',
          plant_code: 'H880',
        },
      ],
      branchAddress: [
        {
          cus_id: this.userData.data.Cus_id,
          branch_code: branch_code,
          add_no: formAddress.add_no,
          soi: formAddress.soi,
          moo: formAddress.moo,
          road: formAddress.road,
          sub_id: formAddress.subdistrict,
          dist_id: formAddress.district,
          pro_id: formAddress.province,
          phone: formAddress.phone,
          email: formAddress.email,
          fax: formAddress.fax,
          contact_name: formAddress.contact_name,
          contact_phone: formAddress.contact_phone,
          contact_email: formAddress.contact_email,
          latitude: this.currentPosition.lat,
          longtitude: this.currentPosition.lng,
        },
      ],
      language: '01',
    };

    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    await loading.present();
    this.apiservice.PostCustomerAddress(this.userData, data).subscribe(
      (response) => {
        let title = '';
        let message = '';
        let state = 0;
        if (response === undefined) {
          title = 'แจ้งเตือน';
          message = 'ไม่สามารถเพิ่มที่อยู่ได้';
          state = 0;
          this.presentAlertConfirm(title, message, state);
          loading.dismiss();
        } else {
          loading.dismiss();
          // const userDetail = JSON.parse(response);
          // this.storageService.setObject('logined', userDetail.data);
          // console.log(userDetail);
          this.router.navigateByUrl('/address');
        }
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );

    console.log(data);
  }

  async getProvince() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.getProvince().subscribe(
      (response) => {
        const listProv = JSON.parse(response);
        this.provList = listProv.data;
        loading.dismiss();
        // console.log(this.provList);
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }
  async getDistrict(provId) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.getDistrict(provId).subscribe(
      (response) => {
        const listData = JSON.parse(response);
        this.distList = listData.data;
        loading.dismiss();
        // console.log(this.provList);
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }
  async getSubDistrict(distId) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.getSubDistrict(distId).subscribe(
      (response) => {
        const listData = JSON.parse(response);
        this.subdistList = listData.data;
        listData.data.forEach((value, key) => {
          this.distCodeGuid[value.Sub_id] = value;
        });
        console.log(this.subdistList);
        loading.dismiss();
        // console.log(this.provList);
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  selectSubDistrict(subDistId) {
    this.currentPosition = { lat: this.distCodeGuid[subDistId].Sub_latitude, lng: this.distCodeGuid[subDistId].Sub_longitude };
    this.currentMarker.setPosition(this.currentPosition);
    this.map.panTo(this.currentPosition);
    this.map.setZoom(13);
  }

  pad = function (pad, str, padLeft) {
    if (typeof str === 'undefined') return pad;
    if (padLeft) {
      return (pad + str).slice(-pad.length);
    } else {
      return (str + pad).substring(0, pad.length);
    }
  };

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
