import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartconfirmPage } from './cartconfirm.page';

const routes: Routes = [
  {
    path: '',
    component: CartconfirmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartconfirmPageRoutingModule {}
