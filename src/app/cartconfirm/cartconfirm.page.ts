import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { StorageService } from '../storage.service';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { GoogleBasemap } from '../class/google-basemap';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { stringify } from '@angular/compiler/src/util';
declare const google: any;
@Component({
  selector: 'app-cartconfirm',
  templateUrl: './cartconfirm.page.html',
  styleUrls: ['./cartconfirm.page.scss'],
})
export class CartconfirmPage implements OnInit {
  public loginForm: FormGroup;
  userData: any;
  product: any;
  cart: any;
  arrArticles: any;
  cartOrder: any;
  today = new Date();
  sCore: any;

  GoogleBasemap = new GoogleBasemap();
  trafficLayer: any;
  map: any;
  shopData: any;
  currentPosition: any;
  addrSelect: any;
  promotionSelect = [];
  discountTotal = 0;
  orderDiscounts = [];

  selectShops = [];
  masterConfig: any;

  public submitAttempt: boolean = true;
  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    private geolocation: Geolocation,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);
    // let watch = this.geolocation.watchPosition();
  }

  ngOnInit() {
    this.map = this.GoogleBasemap.initMap('mapGoogle');
    this.trafficLayer = new google.maps.TrafficLayer();

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
      }

      if (event instanceof NavigationEnd) {
        // // Hide loading indicator
        // this.storageService.getObject('activeAddress').then((result) => {
        //   if (result != null) {
        //     this.addrSelect = result;
        //     console.log(this.addrSelect);
        //     this.addCurrentMarker({ lat: this.addrSelect.Latitude, lng: this.addrSelect.Longtitude });
        //     this.GetPlantAll();
        //   }
        // });
        // this.storageService.getObject('deliveryOrder').then((result) => {
        //   if (result != null) {
        //     this.promotionSelect = result;
        //     result.forEach((e) => {
        //       this.discountTotal += e.discount_amount;
        //     });
        //   }
        // });
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
      }
    });
  }

  goBack() {
    this.router.navigateByUrl('/tabs/cart');
  }

  ionViewWillEnter() {
    this.storageService.getObject('deliveryOrder').then((result) => {
      if (result != null) {
        this.promotionSelect = result;
        result.forEach((e) => {
          this.discountTotal += e.discount_amount;
        });
      }
    });

    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
          console.log(this.userData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('addToCart')
      .then((result) => {
        if (result != null) {
          this.cart = result;
          console.log(result);
          // result.forEach((item, key) => {
          //   if (item.promotion) {
          //     // console.log(item.promotion);
          //     item.promotion.forEach((promo, keyPromo) => {
          //       this.orderDiscounts.push({
          //         order_no: 'auto',
          //         item_no: Number(key + 1),
          //         discount_id: promo.Discount_id,
          //         discount_value: promo.Discount_value,
          //         discount_qty: 1,
          //         discount_amount: promo.Discount_value,
          //       });
          //     });
          //   }
          // });
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('masterConfig')
      .then((result) => {
        this.masterConfig = result;
        console.log(this.masterConfig);
        this.getAddress();
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  getAddress() {
    this.storageService.getObject('activeAddress').then((result) => {
      if (result != null) {
        this.addrSelect = result;
        console.log(this.addrSelect);

        this.addCurrentMarker({ lat: this.addrSelect.Latitude, lng: this.addrSelect.Longtitude });
        this.GetPlantAll();
      }
    });
  }

  addCurrentMarker(coords) {
    this.currentPosition = { lat: coords.lat, lng: coords.lng };

    const image = {
      url: '../../assets/img/pincustomer.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 67),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(20, 67),
    };

    const marker = new google.maps.Marker({
      map: this.map,
      icon: image,
      // draggable: true,
      animation: google.maps.Animation.DROP,
      position: this.currentPosition,
    });

    const cityCircle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      center: this.currentPosition,
      radius: this.masterConfig.Radius_mobile * 1000,
    });

    marker.addListener('dragend', (event) => {
      this.currentPosition = { lat: event.latLng.lat(), lng: event.latLng.lng() };
      this.selectShopInArea(this.shopData);
    });

    marker.addListener('drag', (event) => {
      cityCircle.setOptions({ center: { lat: event.latLng.lat(), lng: event.latLng.lng() } });
    });

    this.map.setCenter(marker.getPosition());
    this.map.setZoom(11);
  }

  async GetPlantAll() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    await loading.present();

    setTimeout(() => {
      this.apiservice.GetPlantAll().subscribe(
        (res) => {
          const data = JSON.parse(res);
          if (data.response.responseCode === '0000') {
            this.shopData = data.data;
            this.addMarkerShop(this.shopData);
            this.selectShopInArea(this.shopData);
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }, 500);
  }

  addMarkerShop(data) {
    const objDist = [];
    const image = {
      url: '../../assets/img/pinpt.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 48),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(24, 48),
    };

    data.forEach((element) => {
      const marker = new google.maps.Marker({
        map: this.map,
        icon: image,
        label: element.Plant_name_th,
        animation: google.maps.Animation.DROP,
        position: { lat: element.Latitude, lng: element.Longtitude },
      });
    });
  }

  selectShopInArea(data) {
    const objDist = [];
    data.forEach((element) => {
      const dist = this.calDistance(this.currentPosition.lat, this.currentPosition.lng, element.Latitude, element.Longtitude, 'K');
      if (dist <= this.masterConfig.Radius_mobile) {
        objDist.push({
          dist: dist,
          data: element,
        });
      }
    });

    // sort by dist
    objDist.sort((a, b) => {
      return a.dist - b.dist;
    });

    const selectShop = [];
    for (let i = 0; i < 5; i++) {
      if (objDist[i] !== undefined) {
        selectShop.push(objDist[i]);
      }
    }

    console.log(selectShop);
    this.selectShops = selectShop;
    this.storageService.setObject('selectShop', selectShop);
  }

  async saveOrder() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();

    let orderDT = [];
    let orderDP = [];
    let Total_amount_inc = 0;
    let Total_amount_ex = 0;
    let Total_vat = 0;
    let Total_discount = 0;
    let Total_deposit = 0;
    let Total_delivery = 0;
    let Total_amount = 0;
    let Total_service = 0;
    let Order_qty = 0;
    // let dataShop = [];
    let dataShopConfirm = [];
    // this.storageService
    //   .getObject('selectShop')
    //   .then((result) => {
    //     if (result != null) {
    //       // dataShop = result;
    //       result.forEach((s) => {
    //         dataShopConfirm.push({
    //           com_code: s.data.Com_code,
    //           plant_code: s.data.Plant_code,
    //           plant_desc: s.data.Plant_name_th,
    //         });
    //       });
    //     }
    //   })
    //   .catch((e) => {
    //     console.log('error: ', e);
    //   });

    this.selectShops.forEach((s) => {
      dataShopConfirm.push({
        com_code: s.data.Com_code,
        plant_code: s.data.Plant_code,
        plant_desc: s.data.Plant_name_th,
      });
    });

    this.cart.forEach((item, key) => {
      let tmpData = {
        order_no: '0',
        item_no: Number(key + 1),
        mat_code: item.Mat_code,
        order_unit: item.Base_unit,
        order_qty: item.prodQty,
        unit_price: item.Price_inc_vat,
        delivery_price: item.Price_delivery,
        discount: item.sumProdDiscount,
        deposit: item.selectType == 1 ? item.Deposit_price + item.Service_price : 0,
        deposit_qty: item.prodQty,
        total_price: (item.Price_inc_vat + item.Price_delivery) * item.prodQty,
        total_amount: (item.Price_inc_vat + item.Price_delivery) * item.prodQty - item.sumProdDiscount,
        item_desc: item.Mat_desc,
        order_type: String(item.selectType),
        active_dep: 'T',
        discountdt: [],
      };

      if (item.promotion) {
        item.promotion.forEach((promo, keyPromo) => {
          tmpData.discountdt.push({
            order_no: 'auto',
            item_no: Number(key + 1),
            discount_id: promo.Discount_id,
            discount_value: promo.Discount_value,
            discount_qty: 1,
            discount_amount: promo.Discount_value,
          });
        });
      }

      orderDT.push(tmpData);

      if (item.selectType == 1) {
        tmpData = {
          order_no: '0',
          item_no: Number(key + 1),
          mat_code: item.Dep_mat_sap,
          order_unit: 'PC',
          order_qty: item.prodQty,
          unit_price: item.Deposit_price,
          delivery_price: 0,
          discount: 0,
          deposit: 0,
          deposit_qty: item.prodQty,
          total_price: item.Deposit_price * item.prodQty,
          total_amount: item.Dep_price * item.prodQty,
          item_desc: '',
          order_type: String(item.selectType),
          active_dep: 'F',
          discountdt: [],
        };

        orderDP.push(tmpData);

        tmpData = {
          order_no: '0',
          item_no: Number(key + 1),
          mat_code: item.Service_mat_sap,
          order_unit: 'PC',
          order_qty: item.prodQty,
          unit_price: item.Service_price,
          delivery_price: 0,
          discount: 0,
          deposit: 0,
          deposit_qty: item.prodQty,
          total_price: item.Service_price * item.prodQty,
          total_amount: item.Service_price * item.prodQty,
          item_desc: '',
          order_type: String(item.selectType),
          active_dep: 'S',
          discountdt: [],
        };

        orderDP.push(tmpData);
      }

      Total_amount_inc += item.Price_inc_vat * item.prodQty;
      Total_amount_ex += item.Price_inc_vat * item.prodQty - (item.Price_inc_vat * item.prodQty * 7) / 107;
      Total_vat += (item.Price_inc_vat * item.prodQty * 7) / 107;
      Total_discount += item.sumProdDiscount;

      //Comment By Paw
      //Total_deposit += item.selectType === 2 ? 0 : item.Deposit_price;
      //Total_delivery += item.Price_delivery;
      //Total_amount += (tmpData.unit_price + tmpData.delivery_price + tmpData.deposit) * item.prodQty - item.sumProdDiscount;

      //Add By Paw
      Total_deposit += item.sumDepPrice;
      Total_delivery += item.sumPriceDelivery;
      Total_service += item.sumServicePrice;
      Total_amount += item.sumProdTotal;

      Order_qty += 1;
    });

    this.cartOrder = {
      orderHD: {
        order_no: 'auto',
        order_date: this.today,
        order_time: this.today,
        cus_id: this.userData?.data?.Cus_id,
        max_id: this.userData?.data?.Max_id,
        cus_tel: '',
        ship_to_desc: this.addrSelect?.address_detail + ' ' + this.addrSelect?.contacttel,
        latitude: this.addrSelect?.Latitude,
        longitude: this.addrSelect?.Longtitude,
        total_amount_inc: Total_amount_inc,
        total_amount_ex: Total_amount_ex,
        total_vat: Total_vat,
        total_discount: Total_discount,
        total_deposit: Total_deposit,
        total_delivery: Total_delivery,
        total_amount: Total_amount,
        total_service: Total_service,
        order_qty: Order_qty,
        remark: 'string',
        status: 'N',
        order_com: '',
        order_plant: '',
        created_user: this.userData?.data?.First_name,
        created_date: this.today,
        edited_user: this.userData?.data?.First_name,
        edited_date: this.today,
        branch_code: this.addrSelect?.Branch_code,
      },
      orderDT: orderDT,
      orderDP: orderDP,
      suggestionPlants: dataShopConfirm,
      // orderDiscounts: this.orderDiscounts,
      language: '02',
    };

    console.log(this.cartOrder);
    // loading.dismiss();

    if (dataShopConfirm.length > 0) {
      this.apiservice
        .saveOrder(this.userData, this.cartOrder)
        .then((response) => {
          if (response !== '') {
            this.storageService.remove('dataProd');
            this.storageService.remove('addToCart');
            console.log('dataProd >>> addToCart');
            this.presentAlertConfirm();
          }
          loading.dismiss();
        })
        .catch(async (e) => {
          const dataErr = JSON.parse(e.error);
          this.presentToast(dataErr?.response?.responseMsg);
          loading.dismiss();
          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบอีกครั้ง.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        });
    } else {
      loading.dismiss();
      const alert = await this.alertCtrl.create({
        header: 'ขออภัย',
        message: 'พื้นที่จัดส่งของท่านอยู่นอกเขตให้บริการ สอบถามเพิ่มเติม ติดต่อ Call Center เบอร์ 02-032-8888',
        cssClass: 'custom-modal',
        buttons: [
          {
            text: 'ยืนยัน',
            handler: () => {
              this.router.navigateByUrl('/tabs/cart');
            },
          },
        ],
        backdropDismiss: false, // <- Here! :)
      });
      await alert.present();
    }
  }

  // async saveOrder() {
  //   const loading = await this.loadingctrl.create({
  //     message: 'Please wait...',
  //     duration: 0,
  //   });
  //   await loading.present();
  //   const dataOrderConfirm = [];
  //   let num = 1;
  //   let Total_amount_inc = 0;
  //   let Total_amount_ex = 0;
  //   let Total_vat = 0;
  //   let Total_discount = 0;
  //   let Total_deposit = 0;
  //   let Total_delivery = 0;
  //   let Total_amount = 0;
  //   let Order_qty = 0;
  //   let dataShopConfirm = [];
  //   this.storageService
  //     .getObject('selectShop')
  //     .then((result) => {
  //       if (result != null) {
  //         let shopData = result;
  //         shopData.forEach((data) => {
  //           dataShopConfirm.push({
  //             com_code: data?.data?.Com_code,
  //             plant_code: data?.data?.Plant_code,
  //             plant_desc: data?.data?.Plant_name_th,
  //           });
  //         });

  //         this.cart.forEach((item) => {
  //           dataOrderConfirm.push({
  //             order_no: '0',
  //             item_no: num.toString(),
  //             mat_code: item.Mat_code,
  //             order_unit: '',
  //             order_qty: 1,
  //             unit_price: item.Price_inc_vat,
  //             delivery_price: item.Price_delivery,
  //             discount: 0,
  //             deposit: item.Dep_price,
  //             deposit_qty: 1,
  //             total_price: item.Price_inc_vat * 1,
  //             total_amount: item.Price_inc_vat * 1 - 0,
  //             item_desc: '',
  //             order_type: '1',
  //           });
  //           Total_amount_inc += 0;
  //           Total_amount_ex += 0;
  //           Total_vat += 0;
  //           Total_discount += 0;
  //           Total_deposit += item.Dep_price;
  //           Total_delivery += item.Price_delivery;
  //           Total_amount += item.Price_inc_vat * 1 - 0;
  //           Order_qty += 1;
  //           num++;
  //         });
  //         this.cartOder = {
  //           orderHD: {
  //             order_no: 'auto',
  //             order_date: this.today,
  //             // order_time: this.today,
  //             cus_id: this.userData?.data?.Cus_id,
  //             max_id: this.userData?.data?.Max_id,
  //             cus_tel: '1131109800',
  //             ship_to_desc: this.addrSelect?.address_detail,
  //             latitude: this.addrSelect?.Latitude,
  //             longitude: this.addrSelect?.Longtitude,
  //             total_amount_inc: Total_amount_inc,
  //             total_amount_ex: Total_amount_ex,
  //             total_vat: Total_vat,
  //             total_discount: Total_discount,
  //             total_deposit: Total_deposit,
  //             total_delivery: Total_delivery,
  //             total_amount: Total_amount,
  //             order_qty: Order_qty,
  //             remark: 'string',
  //             status: 'N',
  //             order_com: '',
  //             order_plant: '',
  //             created_user: this.userData?.data?.Cus_id,
  //             created_date: this.today,
  //             edited_user: this.userData?.data?.Cus_id,
  //             edited_date: this.today,
  //           },
  //           orderDT: dataOrderConfirm,
  //           suggestionPlants: dataShopConfirm,
  //           orderDiscounts: this.promotionSelect,
  //           language: '01',
  //         };

  //         console.log(this.cartOder);

  //         // this.apiservice
  //         //   .saveOrder(this.userData, this.cartOder)
  //         //   .then((response) => {
  //         //     if (response !== '') {
  //         //       this.presentAlertConfirm();
  //         //       loading.dismiss();
  //         //     }
  //         //   })
  //         //   .catch((e) => {
  //         //     const dataErr = JSON.parse(e.error);
  //         //     this.presentToast(dataErr.response.responseMsg);
  //         //     loading.dismiss();
  //         //   });
  //       }
  //     })
  //     .catch((e) => {
  //       console.log('error: ', e);
  //     });
  // }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // :::                                                                         :::
  // :::  This routine calculates the distance between two points (given the     :::
  // :::  latitude/longitude of those points). It is being used to calculate     :::
  // :::  the distance between two locations using GeoDataSource (TM) prodducts  :::
  // :::                                                                         :::
  // :::  Definitions:                                                           :::
  // :::    South latitudes are negative, east longitudes are positive           :::
  // :::                                                                         :::
  // :::  Passed to function:                                                    :::
  // :::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
  // :::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
  // :::    unit = the unit you desire for results                               :::
  // :::           where: 'M' is statute miles (default)                         :::
  // :::                  'K' is kilometers                                      :::
  // :::                  'N' is nautical miles                                  :::
  // :::                                                                         :::
  // :::  Worldwide cities and other features databases with latitude longitude  :::
  // :::  are available at https://www.geodatasource.com                         :::
  // :::                                                                         :::
  // :::  For enquiries, please contact sales@geodatasource.com                  :::
  // :::                                                                         :::
  // :::  Official Web site: https://www.geodatasource.com                       :::
  // :::                                                                         :::
  // :::               GeoDataSource.com (C) All Rights Reserved 2018            :::
  // :::                                                                         :::
  // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  calDistance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == 'K') {
        dist = dist * 1.609344;
      }
      if (unit == 'N') {
        dist = dist * 0.8684;
      }
      return dist;
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'การสั่งซื้อ',
      message: 'คุณทำการสั่งซื้อสินค้าเรียบร้อย',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {
            setTimeout(() => {
              this.router.navigateByUrl('/tabs/orders');
            }, 500);
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }

  pad = function (pad, str, padLeft) {
    if (typeof str === 'undefined') return pad;
    if (padLeft) {
      return (pad + str).slice(-pad.length);
    } else {
      return (str + pad).substring(0, pad.length);
    }
  };
}
