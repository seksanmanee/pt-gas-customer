import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartconfirmPage } from './cartconfirm.page';

describe('CartconfirmPage', () => {
  let component: CartconfirmPage;
  let fixture: ComponentFixture<CartconfirmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartconfirmPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartconfirmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
