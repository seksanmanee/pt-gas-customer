import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartconfirmPageRoutingModule } from './cartconfirm-routing.module';

import { CartconfirmPage } from './cartconfirm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartconfirmPageRoutingModule
  ],
  declarations: [CartconfirmPage]
})
export class CartconfirmPageModule {}
