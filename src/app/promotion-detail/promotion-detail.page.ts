import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.page.html',
  styleUrls: ['./promotion-detail.page.scss'],
})
export class PromotionDetailPage implements OnInit {
  promotionData = {
    Discount_code: '',
    Discount_desc: '',
    Discount_image_name: '',
  };
  getValue: any;
  data: any = {
    Image_uri: '',
  };
  constructor(
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiService: ApiService,
    public loadingctrl: LoadingController,
    public toastController: ToastController,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.route.queryParams.subscribe(async (params) => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);

        // console.log(this.data);

        const loading = await this.loadingctrl.create({
          message: 'Please wait...',
          duration: 0,
        });
        await loading.present();
        this.apiService
          .GetPromotionByID(this.data.Discount_id)
          .then((resp) => {
            const getData = JSON.parse(resp);
            if (getData.response.responseCode === '0000') {
              this.promotionData = getData.data[0];
              // console.log(this.promotionData);
              loading.dismiss();
            } else {
              this.presentToast('ไม่พบข้อมูล Max Card.');
              loading.dismiss();
            }
          })
          .catch((e) => {
            loading.dismiss();
            console.error(e);
          });
      }
    });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
}
