import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegrsterMaxCardPage } from './regrster-max-card.page';

const routes: Routes = [
  {
    path: '',
    component: RegrsterMaxCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegrsterMaxCardPageRoutingModule {}
