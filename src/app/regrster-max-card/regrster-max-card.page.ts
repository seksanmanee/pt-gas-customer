import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-regrster-max-card',
  templateUrl: './regrster-max-card.page.html',
  styleUrls: ['./regrster-max-card.page.scss'],
})
export class RegrsterMaxCardPage implements OnInit {
  public registerForm: FormGroup;
  public condition = false;

  public inputTypeCard = 'text';
  public inputTypeCardMax = 10;
  public inputTypeCardValue = null;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public apiService: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public nav: NavController,
    public storageService: StorageService,
    public toastController: ToastController
  ) {
    this.registerForm = formBuilder.group({
      registerType: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      regisNumber: ['', Validators.compose([Validators.maxLength(40), Validators.required])],
    });
  }

  ngOnInit() {}

  changeCard(e) {
    console.log(e.detail.value);

    switch (Number(e.detail.value)) {
      case 1:
        this.inputTypeCard = 'number';
        this.inputTypeCardMax = 10;
        this.inputTypeCardValue = 0;
        break;

      case 2:
        this.inputTypeCard = 'number';
        this.inputTypeCardMax = 13;
        this.inputTypeCardValue = 0;
        break;

      default:
        this.inputTypeCard = 'text';
        this.inputTypeCardMax = 10;
        this.inputTypeCardValue = null;
        break;
    }

    console.log(this.inputTypeCard, this.inputTypeCardMax, this.inputTypeCardValue);
  }

  acceptCondition() {
    if (this.condition) {
      this.condition = false;
    } else {
      this.condition = true;
    }
  }

  async GetProfileFromMaxCard() {
    // console.log(this.registerForm.value);
    let dataRegis: any;
    if (!this.registerForm.valid) {
      // this.presentAccept();
      console.log('Error!');
    } else {
      const loading = await this.loadingctrl.create({
        message: 'Please wait...',
        duration: 0,
      });
      await loading.present();
      const formRegis = this.registerForm.value;
      // console.log(formRegis);

      this.apiService.GetMIDCustomer().then((res) => {
        const midCus = JSON.parse(res);
        const mid = midCus?.data[0]?.Crm_mid;
        if (midCus.response.responseCode === '0000') {
          this.apiService
            .GetProfileFromMaxCard(formRegis, mid)
            .then((resp) => {
              const maxCardData = JSON.parse(resp);
              if (maxCardData.response.responseCode === '0000') {
                maxCardData.data.mid = mid;
                this.storageService.setObject('maxCardData', maxCardData.data);
                setTimeout(() => {
                  this.router.navigateByUrl('/register');
                }, 100);
                loading.dismiss();
              } else {
                this.presentToast('ไม่พบข้อมูล Max Card.');
                loading.dismiss();
              }
            })
            .catch((e) => {
              const dataErr = JSON.parse(e.error);
              this.presentToast(dataErr.response.responseMsg);
              loading.dismiss();
            });
        } else {
          loading.dismiss();
        }
      });
    }
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
}
