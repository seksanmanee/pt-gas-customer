import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegrsterMaxCardPageRoutingModule } from './regrster-max-card-routing.module';

import { RegrsterMaxCardPage } from './regrster-max-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegrsterMaxCardPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegrsterMaxCardPage]
})
export class RegrsterMaxCardPageModule {}
