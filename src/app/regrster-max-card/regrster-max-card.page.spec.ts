import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegrsterMaxCardPage } from './regrster-max-card.page';

describe('RegrsterMaxCardPage', () => {
  let component: RegrsterMaxCardPage;
  let fixture: ComponentFixture<RegrsterMaxCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegrsterMaxCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegrsterMaxCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
