import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../storage.service';
import { ApiService } from '../api.service';
import { GoogleBasemap } from '../class/google-basemap';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare const google: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public tel_callcenter = { CallCenter_Vertical: null, CallCenter_Horizontal: null };
  GoogleBasemap = new GoogleBasemap();
  trafficLayer: any;
  map: any;
  shopData: any;
  currentPosition: any;
  sCore: any;
  userData: any;
  userImg = '../../assets/img/profile.png';
  arrArticles: any;
  arrVideos: any;

  userProfile: any;

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public alertCtrl: AlertController,
    public storageService: StorageService,
    public loadingctrl: LoadingController,
    private apiService: ApiService,
    private geolocation: Geolocation,
    public toastController: ToastController
  ) {
    this.storageService
      .getObject('logined')
      .then(async (result) => {
        console.log(result);
        if (result != null) {
          this.userData = result;
          this.GetScore();
        } else {
          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบก่อน.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // let watch = this.geolocation.watchPosition();
  }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
          console.log(this.userData);
          // this.GetUserProfile(this.userData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('Tel_callcenter')
      .then(async (result) => {
        console.log(result);
        if (result) {
          this.tel_callcenter = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.map = this.GoogleBasemap.initMap('mapGoogle');
    this.trafficLayer = new google.maps.TrafficLayer();

    // this.geolocation.getCurrentPosition().then((resp) => {
    //   // resp.coords.latitude
    //   // resp.coords.longitude
    //   // console.log("geolocation", resp.coords.latitude);
    //   this.addCurrentMarker(resp.coords);

    // }).catch((error) => {
    //   console.log('Error getting location', error);
    // });

    this.addCurrentMarker({ lat: 13.767, lng: 100.5668656616 });

    this.GetPlantAll();
  }

  // GetUserProfile(userData) {
  //   this.apiService
  //     .GetUserProfile(userData)
  //     .then((resp) => {
  //       this.userProfile = JSON.parse(resp);
  //       console.log(this.userProfile);
  //     })
  //     .catch((e) => {
  //       console.log(e);
  //     });
  // }

  addCurrentMarker(coords) {
    this.currentPosition = { lat: 13.767, lng: 100.5668656616 };
    const image = {
      url: '../../assets/img/pincustomer.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 67),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(20, 67),
    };

    let marker = new google.maps.Marker({
      map: this.map,
      icon: image,
      // draggable: true,
      animation: google.maps.Animation.DROP,
      // position: { lat: coords.latitude, lng: coords.longitude }
      position: this.currentPosition,
    });

    // const cityCircle = new google.maps.Circle({
    //   strokeColor: '#FF0000',
    //   strokeOpacity: 0.8,
    //   strokeWeight: 2,
    //   fillColor: '#FF0000',
    //   fillOpacity: 0.35,
    //   map: this.map,
    //   center: this.currentPosition,
    //   radius: 5000,
    // });

    // marker.addListener('dragend', (event) => {
    //   this.currentPosition = { lat: event.latLng.lat(), lng: event.latLng.lng() };
    //   // this.selectShopInArea(this.shopData);
    // });

    // marker.addListener('drag', (event) => {
    //   cityCircle.setOptions({ center: { lat: event.latLng.lat(), lng: event.latLng.lng() } });
    // });

    this.map.setCenter(marker.getPosition());
    this.map.setZoom(11);
  }

  async GetPlantAll() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    await loading.present();

    setTimeout(() => {
      this.apiService.GetPlantAll().subscribe(
        (res) => {
          const data = JSON.parse(res);
          if (data.response.responseCode === '0000') {
            this.shopData = data.data;
            // console.log(this.shopData);
            this.addMarkerShop(this.shopData);
            // this.selectShopInArea(this.shopData);
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }, 500);
  }

  addMarkerShop(data) {
    const objDist = [];
    const image = {
      url: '../../assets/img/pinpt.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 48),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(24, 48),
    };
    data.forEach((element) => {
      let marker = new google.maps.Marker({
        map: this.map,
        icon: image,
        animation: google.maps.Animation.DROP,
        position: { lat: element.Latitude, lng: element.Longtitude },
      });
    });
  }

  // selectShopInArea(data) {
  //   const objDist = [];
  //   data.forEach(element => {
  //     const dist = this.calDistance(this.currentPosition.lat, this.currentPosition.lng, element.Latitude, element.Longtitude, 'K');
  //     if (dist <= 5) {
  //       objDist.push({
  //         dist: dist,
  //         data: element
  //       });
  //     }
  //   });

  //   // sort by dist
  //   objDist.sort( (a, b) => {
  //     return a.dist - b.dist;
  //   });

  //   let selectShop = [];
  //   for (let i = 0; i < 5; i++) {
  //     if (objDist[i] !== undefined) {
  //       selectShop.push(objDist[i]);
  //     }
  //   }

  //   console.log(selectShop);
  //   this.storageService.setObject('selectShop', selectShop);

  // }

  editProfile() {
    this.router.navigateByUrl('/editprofile');
  }

  seeArticleDetail(id: string) {
    this.nav.navigateForward('/articledetail/' + id);
  }

  seeVideoDetail(id: string) {
    this.nav.navigateForward('/videobookmark/' + id);
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Are you sure you want to logout ?',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Okay',
          handler: () => {
            this.storageService.clear();
            this.router.navigateByUrl('/welcome');
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  async GetScore() {
    this.apiService.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiService
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }

  // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // :::                                                                         :::
  // :::  This routine calculates the distance between two points (given the     :::
  // :::  latitude/longitude of those points). It is being used to calculate     :::
  // :::  the distance between two locations using GeoDataSource (TM) prodducts  :::
  // :::                                                                         :::
  // :::  Definitions:                                                           :::
  // :::    South latitudes are negative, east longitudes are positive           :::
  // :::                                                                         :::
  // :::  Passed to function:                                                    :::
  // :::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
  // :::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
  // :::    unit = the unit you desire for results                               :::
  // :::           where: 'M' is statute miles (default)                         :::
  // :::                  'K' is kilometers                                      :::
  // :::                  'N' is nautical miles                                  :::
  // :::                                                                         :::
  // :::  Worldwide cities and other features databases with latitude longitude  :::
  // :::  are available at https://www.geodatasource.com                         :::
  // :::                                                                         :::
  // :::  For enquiries, please contact sales@geodatasource.com                  :::
  // :::                                                                         :::
  // :::  Official Web site: https://www.geodatasource.com                       :::
  // :::                                                                         :::
  // :::               GeoDataSource.com (C) All Rights Reserved 2018            :::
  // :::                                                                         :::
  // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  calDistance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 === lat2 && lon1 === lon2) {
      return 0;
    } else {
      const radlat1 = (Math.PI * lat1) / 180;
      const radlat2 = (Math.PI * lat2) / 180;
      const theta = lon1 - lon2;
      const radtheta = (Math.PI * theta) / 180;
      let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit === 'K') {
        dist = dist * 1.609344;
      }
      if (unit === 'N') {
        dist = dist * 0.8684;
      }
      return dist;
    }
  }
}
