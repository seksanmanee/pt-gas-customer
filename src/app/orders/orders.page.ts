import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  userData: any;
  order: any;
  sCore: any;

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public alertCtrl: AlertController,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public storageService: StorageService,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);
  }

  ngOnInit() {
    // this.loadData();
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('logined')
      .then(async (result) => {
        console.log(result);
        if (result !== null) {
          this.userData = result;
          this.GetScore();
          this.loadData();
        } else {
          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบก่อน.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // setTimeout(() => {
    //   this.loadData();
    // }, 1000);
  }

  async loadData() {
    if (this.userData !== undefined) {
      const loading = await this.loadingctrl.create({
        message: 'Please wait...',
        duration: 0,
      });

      await loading.present();

      this.apiservice
        .OrderList(this.userData)
        .then((res: any) => {
          const data = JSON.parse(res);
          if (data.response.responseCode === '0000') {
            this.order = data.data;
            console.log(this.order);
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        })
        .catch(async (e) => {
          loading.dismiss();
          console.log(e);

          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบอีกครั้ง.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        });
    }
  }

  convertStatusText(status) {
    let statusText = '';
    switch (status) {
      case 'N':
        statusText = 'รอยืนยันรายการสั่งซื้อ';
        break;
      case 'W':
        statusText = 'อยู่ระหว่างการจัดส่ง';
        break;
      case 'D':
        statusText = 'จัดส่งเรียบร้อย';
        break;
      case 'S':
        statusText = 'ชำระเงินเรียบร้อย';
        break;
      case 'C':
        statusText = 'ยกเลิก';
        break;
    }
    return statusText;
  }

  gotoDetail(selectOrder) {
    this.storageService.setObject('selectOrder', selectOrder);
    setTimeout(() => {
      this.router.navigateByUrl('/orderdetail');
    }, 100);
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
