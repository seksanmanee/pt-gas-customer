import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  public submitAttempt: boolean = false;
  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public apiservice: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public storageService: StorageService,
    private iab: InAppBrowser
  ) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required],
      language: '02',
    });
  }

  ngOnInit() {}

  async goLogin() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log('Error!');
    } else {
      const loading = await this.loadingctrl.create({
        message: 'Please wait...',
        duration: 0,
      });
      await loading.present();
      // console.log(this.loginForm.value);
      this.apiservice.UsersLogin(this.loginForm.value).subscribe(
        (response) => {
          let title: string = '';
          let message: string = '';
          let state: number = 0;
          if (response === undefined) {
            title = 'แจ้งเตือน';
            message = 'ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบ Username / Password';
            state = 0;
            this.presentAlertConfirm(title, message, state);
            loading.dismiss();
          } else {
            loading.dismiss();
            const userDetail = JSON.parse(response);
            this.storageService.setObject('logined', userDetail.data);
            // console.log(userDetail);

            this.apiservice
              .GetMasterConfig(userDetail.data)
              .then((resp) => {
                const getData = JSON.parse(resp);
                const Tel_callcenter = {
                  CallCenter_Horizontal: getData.data[0].CallCenter_Horizontal,
                  CallCenter_Vertical: getData.data[0].CallCenter_Vertical,
                };
                this.storageService.setObject('Tel_callcenter', Tel_callcenter);
                // console.log(Tel_callcenter);
                this.storageService.setObject('masterConfig', getData.data[0]);
                // console.log(getData.data[0]);
                setTimeout(() => {
                  this.router.navigateByUrl('/tabs');
                }, 500);
              })
              .catch((e) => {
                console.error(e);
              });

            // this.router.navigateByUrl('/tabs');
          }
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }
  }
  goBack() {
    this.router.navigateByUrl('/tabs');
  }
  goRegisterType() {
    this.router.navigateByUrl('/registertype');
  }
  goRegister() {
    this.storageService.remove('maxCardData');
    setTimeout(() => {
      this.router.navigateByUrl('/register');
    }, 500);
  }
  goHome() {
    this.router.navigateByUrl('/tabs');
  }
  goForgotpassword() {
    // this.router.navigateByUrl('/forgotpassword');
    this.iab.create('https://asv-mobile-phase1-api-dev.azurewebsites.net/ManagementAccount/ForgotPassword').show();
  }
  async presentAlertConfirm(title: string, message: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: message,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  goRegisterMacCard() {
    this.router.navigateByUrl('/regrster-max-card');
    console.log('register mac card');
  }
}
