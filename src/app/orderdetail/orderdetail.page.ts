import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-orderdetail',
  templateUrl: './orderdetail.page.html',
  styleUrls: ['./orderdetail.page.scss'],
})
export class OrderdetailPage implements OnInit {
  selectOrder: any;
  userData: any;
  showPriceDetail = false;
  product: any;
  cart: any;
  arrArticles: any;
  sCore: any;
  dataOrders = {
    Order_no: null,
    Order_date: null,
    status_desc: null,
    Order_qty: null,
    Total_delivery: null,
    Total_deposit: null,
    Total_discount: null,
    Total_amount: null,
    Ship_to_desc: null,
    Latitude: null,
    Longitude: null,
    sale_total_amount: null,
    sale_total_disamount: null,
    Cus_id: null,
    customer_Name: null,
    Max_id: null,
    Order_com: null,
    Company_Name: null,
    Order_plant: null,
    Plant_name_th: null,
    Status: null,
    order_dt: [],
    Status_desc: null,
  };

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator

    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadOrder();
    this.showPriceDetail = false;
  }

  gotoOrderDirectionMap() {
    this.router.navigateByUrl('order-direction-map');
  }

  loadOrder() {
    this.storageService
      .getObject('selectOrder')
      .then(async (result) => {
        if (result != null) {
          this.selectOrder = result;

          const loading = await this.loadingctrl.create({
            message: 'Please wait...',
            duration: 0,
          });
          await loading.present();
          this.apiservice.OrderingByOrderId(this.selectOrder).subscribe(
            (res) => {
              const data = JSON.parse(res);
              console.log(data);
              if (data.response.responseCode === '0000') {
                this.dataOrders = data.data[0];
                this.storageService.setObject('orderDetail', data.data[0]);
                console.log(this.dataOrders);
              } else {
                this.presentToast(data.response.responseMsg);
              }
              loading.dismiss();
            },
            (error) => {
              loading.dismiss();
              console.log(error);
            }
          );
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  convertStatusText(status) {
    let statusText = '';
    switch (status) {
      case 'N':
        statusText = 'รอยืนยันรายการสั่งซื้อ';
        break;
      case 'W':
        statusText = 'อยู่ระหว่างการจัดส่ง';
        break;
      case 'D':
        statusText = 'จัดส่งเรียบร้อย';
        break;
      case 'S':
        statusText = 'ชำระเงินเรียบร้อย';
        break;
      case 'C':
        statusText = 'ยกเลิก';
        break;
    }
    return statusText;
  }

  async changeSendStatus(status) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();

    this.apiservice.UpdateStatusOrderingOnline(this.userData, this.dataOrders.Order_no, status).subscribe(
      (res) => {
        const data = JSON.parse(res);
        console.log(data);
        if (data.response.responseCode === '0000') {
          this.presentToast(data.response.responseMsg);
          setTimeout(() => {
            if (status === 2) {
              this.gotoPayment();
            } else {
              this.router.navigateByUrl('/tabs/home');
            }
          }, 100);
        } else {
          this.presentToast(data.response.responseMsg);
        }
        loading.dismiss();
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  gotoPayment() {
    let checkScanQr = false;
    this.dataOrders.order_dt.forEach((element) => {
      if (element.Order_unit === 'KG') {
        checkScanQr = true;
      }
    });

    if (!checkScanQr) {
      this.router.navigateByUrl('order-payment');
    } else {
      this.storageService
        .getObject('qrscan')
        .then((result) => {
          if (result != null) {
            this.router.navigateByUrl('order-payment');
          } else {
            this.presentToast('กรุณา Scan ถังแก๊สก่อน!');
          }
        })
        .catch((e) => {
          console.log('error: ', e);
        });
    }
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }

}
