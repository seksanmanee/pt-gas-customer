import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController, ToastController } from '@ionic/angular';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../storage.service';
import { ApiService } from '../api.service';
import { GoogleBasemap } from '../class/google-basemap';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-selectpromotion',
  templateUrl: './selectpromotion.page.html',
  styleUrls: ['./selectpromotion.page.scss'],
})
export class SelectpromotionPage implements OnInit {
  userData: any;
  sCore: any;
  promotionData: any;
  dataDiscount = [];
  Mat_code: any;
  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public alertCtrl: AlertController,
    public storageService: StorageService,
    public loadingctrl: LoadingController,
    public apiService: ApiService,
    private geolocation: Geolocation,
    public toastController: ToastController
  ) {
    this.Mat_code = this.router.getCurrentNavigation().extras.state.Mat_code;

    console.log(this.Mat_code);

    this.storageService.getObject('logined').then((result) => {
      if (result != null) {
        this.userData = result;
        this.GetScore();
      }
    });
  }
  async ngOnInit() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    await loading.present();

    setTimeout(() => {
      this.apiService.GetPromotionByMaterial(this.userData, this.Mat_code).subscribe(
        (res) => {
          const data = JSON.parse(res);

          console.log(data);

          if (data.response.responseCode === '0000') {
            // this.promotionData = data.data;

            let tmpData = data.data;
            const promoSelect = [];
            this.storageService.getObject('selectPromotion').then((result) => {
              if (result) {
                result.forEach((value, key) => {
                  promoSelect.push(value.Discount_id);
                });
              }

              data.data.forEach((value, key) => {
                if (promoSelect.indexOf(value.Discount_id) > -1) {
                  tmpData[key].active = true;
                } else {
                  tmpData[key].active = false;
                }
              });

              if (this.userData.data.Max_id === '') {
                tmpData = tmpData.filter((data, key) => {
                  return data.Crm_promo_code === '';
                });
              }

              // console.log(tmpData);

              this.promotionData = tmpData;

              // console.log(this.promotionData);
            });
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }, 500);

    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator
    //     this.dataDiscount = [];
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });
  }

  ionViewWillEnter() {
    this.dataDiscount = [];
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
  addToOrder() {
    this.router.navigateByUrl('/tabs/cart');
  }
  selectAddrToOrder(id, value) {
    const rNum = this.dataDiscount.length;
    const dt = {
      order_no: 'auto',
      item_no: rNum + 1,
      discount_id: id,
      discount_value: value,
      discount_qty: 1,
      discount_amount: value,
    };
    this.dataDiscount.push(dt);
    this.storageService.setObject('deliveryOrder', this.dataDiscount);
    //console.log(this.dataDiscount);
  }

  selectPromotion(data) {
    this.storageService.getObject('selectPromotion').then((result) => {
      const promoSelect = [];
      if (result) {
        result.forEach((value, key) => {
          promoSelect.push(value.Discount_id);
        });
      }

      // console.log(result.length);
      const promo = this.promotionData;
      const tmpData = [];

      promo.forEach((value, key) => {
        if (data.Discount_id === value.Discount_id) {
          if (promoSelect.indexOf(data.Discount_id) > -1) {
            promo[key].active = false;
          } else {
            promo[key].active = true;
          }
        }

        if (promo[key].active === true) {
          tmpData.push(promo[key]);
        }
      });

      this.storageService.setObject('selectPromotion', tmpData);
      this.promotionData = promo;
      setTimeout(() => {
        this.router.navigateByUrl('/product');
      }, 500);
    });
  }

  async GetScore() {
    this.apiService.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiService
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
