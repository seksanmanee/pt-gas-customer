import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectpromotionPage } from './selectpromotion.page';

describe('SelectpromotionPage', () => {
  let component: SelectpromotionPage;
  let fixture: ComponentFixture<SelectpromotionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectpromotionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectpromotionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
