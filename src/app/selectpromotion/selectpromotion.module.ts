import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectpromotionPageRoutingModule } from './selectpromotion-routing.module';

import { SelectpromotionPage } from './selectpromotion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectpromotionPageRoutingModule
  ],
  declarations: [SelectpromotionPage]
})
export class SelectpromotionPageModule {}
