import { Component } from '@angular/core';

import { AlertController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavigationEnd, Event, NavigationError, NavigationStart, Router } from '@angular/router';
import { StorageService } from './storage.service';
import { MenuController } from '@ionic/angular';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public tel_callcenter = { CallCenter_Vertical: null, CallCenter_Horizontal: null };
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'หน้าหลัก',
      url: '/tabs',
      icon: 'home',
    },
    // {
    //   title: 'เพิ่มที่อยู่',
    //   url: '/address',
    //   icon: 'ion-ios-location'
    // },
    {
      title: 'เข้าสู่ระบบ',
      url: '/login',
      icon: 'paper-plane',
    },
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public alertCtrl: AlertController,
    public storageService: StorageService,
    private screenOrientation: ScreenOrientation,
    private menu: MenuController
  ) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
        this.initializeApp();
      }
      if (event instanceof NavigationEnd) {
        // Hide loading indicator
      }
      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        console.log(event.error);
      }
    });

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.storageService.getObject('logined').then(result => {
      //   if (result != null) {
      //     this.router.navigateByUrl('/tabs');
      //   }
      // }).catch(e => {
      //   this.router.navigateByUrl('/login');
      // });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.telcallcenter();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('/')[1];
    // console.log(path);
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex((page) => page.title.toLowerCase() === path.toLowerCase());
    }
  }

  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    alert.present();
  }

  telcallcenter() {
    this.storageService
      .getObject('Tel_callcenter')
      .then(async (result) => {
        // console.log(result);
        if (result) {
          this.tel_callcenter = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // setTimeout(() => {
    //   this.loadData();
    // }, 1000);
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'ยืนยัน!',
      message: 'คุณต้องการออกจากระบบใช่หรือไม่ ?',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.storageService.clear();
            this.router.navigate(['/login']);
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }
  async querySelectorAll() {}
}
