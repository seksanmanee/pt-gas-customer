import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { GoogleBasemap } from '../class/google-basemap';
import { StorageService } from '../storage.service';
declare const google: any;
@Component({
  selector: 'app-order-direction-map',
  templateUrl: './order-direction-map.page.html',
  styleUrls: ['./order-direction-map.page.scss'],
})
export class OrderDirectionMapPage implements OnInit {
  userData: any;
  GoogleBasemap = new GoogleBasemap();
  trafficLayer: any;
  map: any;
  orderDetail: any;
  sCore: any;

  constructor(private router: Router, private menuCtrl: MenuController, public apiService: ApiService, public storageService: StorageService) {
    this.menuCtrl.enable(true);
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.GetScore();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        this.storageService
          .getObject('orderDetail')
          .then((result) => {
            if (result != null) {
              this.orderDetail = result;
              console.log(this.orderDetail);

              this.getPlantAll();
            }
          })
          .catch((e) => {
            console.log('error: ', e);
          });
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
      }
    });
  }

  ngOnInit() {
    this.map = this.GoogleBasemap.initMap('mapGoogle');
    this.trafficLayer = new google.maps.TrafficLayer();
  }

  getPlantAll() {
    this.apiService.GetPlantAll().subscribe(
      (res) => {
        const data = JSON.parse(res);
        if (data.response.responseCode === '0000') {
          data.data.forEach((value, key) => {
            if (value.Plant_code == this.orderDetail.Order_plant) {
              const origin = { lat: value.Latitude, lng: value.Longtitude };
              const destination = { lat: this.orderDetail.Latitude, lng: this.orderDetail.Longitude };
              this.initMap(origin, destination);
            }
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ionViewWillEnter() {
    // this.initMap(origin, destination);
  }

  initMap(origin, destination) {
    var icons = {
      start: new google.maps.MarkerImage(
        // URL
        '../../assets/img/pinpt.png',
        // (width,height)
        new google.maps.Size(40, 48),
        // The origin point (x,y)
        new google.maps.Point(0, 0),
        // The anchor point (x,y)
        new google.maps.Point(24, 48)
      ),
      end: new google.maps.MarkerImage(
        // URL
        '../../assets/img/pincustomer.png',
        // (width,height)
        new google.maps.Size(40, 67),
        // The origin point (x,y)
        new google.maps.Point(0, 0),
        // The anchor point (x,y)
        new google.maps.Point(20, 67)
      ),
    };

    const directionsRenderer = new google.maps.DirectionsRenderer();
    const directionsService = new google.maps.DirectionsService();

    directionsRenderer.setMap(this.map);
    directionsRenderer.setPanel(document.getElementById('mapGoogleDirection'));

    var directionsRequest = {
      origin: origin,
      destination: destination,
      travelMode: google.maps.DirectionsTravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
    };

    directionsService.route(
      directionsRequest,

      (response, status) => {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsRenderer.setDirections(response);
          directionsRenderer.setOptions({ suppressMarkers: true });
          new google.maps.DirectionsRenderer({
            map: this.map,
            directions: response,
            suppressMarkers: true,
          });
          var leg = response.routes[0].legs[0];
          this.makeMarker(leg.start_location, icons.start, 'title', this.map);
          this.makeMarker(leg.end_location, icons.end, 'title', this.map);
        } else {
          alert('Unable to retrive route');
        }
      }
    );

    // directionsService.route(
    //   {
    //     origin: origin,
    //     destination: destination,
    //     travelMode: google.maps.TravelMode.DRIVING,
    //   },
    //   (response, status) => {
    //     if (status === 'OK') {
    //       directionsRenderer.setDirections(response);
    //     } else {
    //       window.alert('Directions request failed due to ' + status);
    //     }
    //   }
    // );
  }

  makeMarker(position, icon, title, map) {
    new google.maps.Marker({
      position: position,
      map: map,
      icon: icon,
      title: title,
    });
  }

  async GetScore() {
    this.apiService.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        const cid = this.userData?.data?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        console.log(cid, mid);
        this.apiService
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }
}
