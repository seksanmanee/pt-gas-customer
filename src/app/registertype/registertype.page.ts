import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { AlertController, LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-registertype',
  templateUrl: './registertype.page.html',
  styleUrls: ['./registertype.page.scss'],
})
export class RegistertypePage implements OnInit {
  constructor(
    private router: Router,
    public apiService: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public nav: NavController,
    public storageService: StorageService
  ) {
  }

  ngOnInit() {
  }

  goBack() {
    this.router.navigateByUrl('/tabs');
  }
  goRegister(){
    this.router.navigateByUrl('/register');
  }
}
