import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistertypePage } from './registertype.page';

const routes: Routes = [
  {
    path: '',
    component: RegistertypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistertypePageRoutingModule {}
