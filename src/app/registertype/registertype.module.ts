import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistertypePageRoutingModule } from './registertype-routing.module';

import { RegistertypePage } from './registertype.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistertypePageRoutingModule
  ],
  declarations: [RegistertypePage]
})
export class RegistertypePageModule {}
