import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistertypePage } from './registertype.page';

describe('RegistertypePage', () => {
  let component: RegistertypePage;
  let fixture: ComponentFixture<RegistertypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistertypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistertypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
