import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../storage.service';
import { ApiService } from '../api.service';
import { GoogleBasemap } from '../class/google-basemap';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-selectaddress',
  templateUrl: './selectaddress.page.html',
  styleUrls: ['./selectaddress.page.scss'],
})
export class SelectaddressPage implements OnInit {
  userData: any;
  addressData: any;
  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public alertCtrl: AlertController,
    public storageService: StorageService,
    public loadingctrl: LoadingController,
    public apiService: ApiService,
    private geolocation: Geolocation,
    public toastController: ToastController
  ) {
    this.storageService.getObject('logined').then((result) => {
      if (result != null) {
        this.userData = result;
      }
    });
  }

  async ngOnInit() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    await loading.present();

    setTimeout(() => {
      this.apiService
        .GetCustomerAddress(this.userData)
        .then(async (res: string) => {
          const data = JSON.parse(res);
          if (data.response.responseCode === '0000') {
            this.addressData = data.data;
            console.log(this.addressData);
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        })
        .catch(async (error) => {
          loading.dismiss();
          console.log(error);

          const alert = await this.alertCtrl.create({
            header: 'ไม่สามารถใช้งานได้',
            message: 'กรุณาเข้าสู่ระบบก่อน.',
            cssClass: 'custom-modal',
            buttons: [
              {
                text: 'ยืนยัน',
                handler: () => {
                  this.router.navigateByUrl('/login');
                },
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        });
    }, 500);
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
  addToOrder() {
    this.router.navigateByUrl('/tabs/cart');
  }
  selectAddrToOrder(addr) {
    this.storageService.setObject('addrDelivery', addr);
  }
}
