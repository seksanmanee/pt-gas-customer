import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectaddressPageRoutingModule } from './selectaddress-routing.module';

import { SelectaddressPage } from './selectaddress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectaddressPageRoutingModule
  ],
  declarations: [SelectaddressPage]
})
export class SelectaddressPageModule {}
