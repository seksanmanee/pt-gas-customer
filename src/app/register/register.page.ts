import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { GoogleBasemap } from '../class/google-basemap';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare const google: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public registerForm: FormGroup;
  public submitAttempt = false;
  public condition = false;
  provList: any;
  distList: any;
  subdistList: any;
  zipcode = '';
  registerType = '01';
  maxCardData = {
    ActivateDate: '',
    ActivateShop: '',
    CarType: '',
    CardNo: '',
    CardStatus: '',
    CardType: '',
    CitizenID: '',
    CitizenType: '1',
    CustomerStatus: '',
    FName: '',
    LName: '',
    NormalPoint: '',
    PhoneNo: '',
    SpecialLimit: '',
    SpecialPoint: '',
    WsTransID: '',
    mid: '',
  };
  GoogleBasemap = new GoogleBasemap();
  trafficLayer: any;
  map: any;
  currentPosition: any;
  prefixsList: any;
  prefixsDesc = {};

  provCodeGuid = {};
  ampCodeGuid = {};
  distCodeGuid = {};
  currentMarker: any;

  public inputTypeCard = 'number';
  public inputTypeCardMax = 13;
  public inputTypeCardValue = null;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public apiService: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public nav: NavController,
    private geolocation: Geolocation,
    public storageService: StorageService,
    public toastController: ToastController
  ) {
    this.registerForm = formBuilder.group({
      prefixs: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
      first_name: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      last_name: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      //card_type: ['', Validators.required],
      card_type: [''],
      //id_card: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      //birthday: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
      //gender: ['', Validators.compose([Validators.maxLength(20), Validators.required])],
      id_card: ['', Validators.compose([Validators.maxLength(100)])],
      birthday: ['', Validators.compose([Validators.maxLength(50)])],
      gender: ['', Validators.compose([Validators.maxLength(20)])],
      phone: ['', Validators.compose([Validators.maxLength(10), Validators.required])],
      // email: ['', Validators.compose([Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'), Validators.required])],
      email: [''],
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required],
      //addrNo: ['',Validators.required],
      //village: ['',Validators.required],
      //moo: ['',Validators.required],
      //soi: ['',Validators.required],
      //road: ['',Validators.required],
      addrNo: [''],
      village: [''],
      moo: [''], 
      soi: [''],
      road: [''],
      province: ['',Validators.required],
      district: ['',Validators.required],
      subdistrict: ['',Validators.required],
      // zipcode: [''],
    });

    let watch = this.geolocation.watchPosition();
  }

  ngOnInit() {
    this.map = this.GoogleBasemap.initMap('mapGoogle');
    this.trafficLayer = new google.maps.TrafficLayer();

    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        // console.log("geolocation", resp.coords.latitude + " >> " + resp.coords.longitude);
        this.addCurrentMarker(resp.coords);
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });

    this.storageService
      .getObject('maxCardData')
      .then((result) => {
        if (result != null) {
          this.maxCardData = result;
          console.log(this.maxCardData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
    this.getProvince();
    this.getPrefixs();
  }

  changeCard(e) {
    // console.log(e.detail.value);
    // switch (Number(e.detail.value)) {
    //   case 1:
    //     this.inputTypeCard = 'number';
    //     this.inputTypeCardMax = 13;
    //     this.inputTypeCardValue = null;
    //     break;
    //   default:
    //     this.inputTypeCard = 'text';
    //     this.inputTypeCardMax = 9;
    //     this.inputTypeCardValue = '';
    //     break;
    // }
    // console.log(this.inputTypeCard, this.inputTypeCardMax, this.inputTypeCardValue);
  }

  ionViewWillEnter() {}

  addCurrentMarker(coords) {
    this.currentPosition = { lat: coords.latitude, lng: coords.longitude };
    const image = {
      url: '../../assets/img/pincustomer.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 67),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(20, 67),
    };

    this.currentMarker = new google.maps.Marker({
      map: this.map,
      icon: image,
      draggable: true,
      animation: google.maps.Animation.DROP,
      // position: { lat: coords.latitude, lng: coords.longitude }
      position: this.currentPosition,
    });

    this.currentMarker.addListener('dragend', (event) => {
      this.currentPosition = { lat: event.latLng.lat(), lng: event.latLng.lng() };
      console.log(this.currentPosition);
    });

    this.map.setCenter(this.currentMarker.getPosition());
    this.map.setZoom(11);
  }

  acceptCondition() {
    this.condition = !this.condition;
  }

  goBack() {
    this.router.navigateByUrl('/tabs');
  }

  regisMaxCard() {
    if (this.registerType === '01') {
      this.registerType = '00';
    } else {
      this.registerType = '01';
    }
  }

  async presentAlertLogin(title: string, msg: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: msg,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  Save() {
    this.apiService
      .GetMIDCustomer()
      .then((res) => {
        const midCus = JSON.parse(res);
        const mid = midCus?.data[0]?.Crm_mid;
        if (midCus.response.responseCode === '0000') {
          this.postRegister(mid);
        } else {
          // loading.dismiss();
          console.error(midCus.response);
        }
      })
      .catch((e) => {
        console.error(e);
      });
  }

  async postRegister(mid) {
    // console.log(this.registerForm.value);
    const formRegis = this.registerForm.value;
    if (formRegis.password !== formRegis.confirmpassword) {
      this.presentAccept('รหัสผ่านไม่ตรงกัน');
    } else {
      let dataRegis: any;
      this.submitAttempt = true;
      if (this.registerForm.valid === false) {
        this.presentAccept('กรุณากรอกข้อมูลในช่องที่มีเครื่องหมายดอกจัน (*) ให้ครบ');
        // console.log('Error!');
      } else {
        if (!this.condition) {
          this.presentAccept('กรุณาตรวจสอบเงื่อนไขและยอมรับ');
        } else {
          const loading = await this.loadingctrl.create({
            message: 'Please wait...',
            duration: 0,
          });
          await loading.present();
          const today = new Date();
          console.log(this.subdistList);

          if(formRegis.id_card===null)
          {
            formRegis.id_card = '';
          }

          dataRegis = {
            customerProfile: {
              account_id: 'auto',
              user_name: formRegis.username,
              password: formRegis.confirmpassword,
              mid: mid,
              tid: mid,
              card_type_id: Number(formRegis.card_type),
              card_id: String(formRegis.id_card),
              maxCard_id: String(this.maxCardData.CardNo),
              title_name_id: Number(formRegis.prefixs),
              title_name_desc: this.prefixsDesc[formRegis.prefixs],
              gender_id: Number(formRegis.gender),
              fname_th: formRegis.first_name,
              lname_th: formRegis.last_name,
              marital_status: 1,
              birth_date: formRegis.birthday,
              add_no: formRegis.addrNo,
              moo: String(formRegis.moo),
              soi: formRegis.soi,
              road: formRegis.road,
              sub_id: formRegis.subdistrict,
              dist_id: formRegis.district,
              pro_id: formRegis.province,

              addr_org_sub_district_id: this.distCodeGuid[formRegis.subdistrict]?.Code_guid,
              addr_org_amphure_id: this.ampCodeGuid[formRegis.district]?.Code_guid,
              addr_org_province_id: this.provCodeGuid[formRegis.province]?.Code_guid,
              addr_org_postalcode_id: this.distCodeGuid[formRegis.subdistrict]?.Zipcode_guid,

              user_email: formRegis.email,
              phone_no: formRegis.phone,
              regis_date: today,
              latitude: this.currentPosition.lat,
              longitude: this.currentPosition.lng,
            },
            register_type: this.registerType,
            language: '01',
          };

          console.log(dataRegis);
          this.apiService
            .addUsers(dataRegis)
            .then((response) => {
              console.log(response);
              const res = JSON.parse(response);
              console.log(res);
              if (res.response.responseCode === '0000') {
                this.presentAlertConfirm(res.data.MaxCard);
                loading.dismiss();
                this.presentToast(res.response.responseMsg);
                this.storageService.remove('maxCardData');
                setTimeout(() => {
                  this.router.navigateByUrl('/login');
                }, 500);
              } else {
                this.presentAccept('ไม่สามารถสมัครสมาชิกได้กรุณาลองใหม่อีกครั้ง');
                loading.dismiss();
              }
            })
            .catch((e) => {
              loading.dismiss();
              if (e?.error?.title) {
                this.presentAccept(e?.error?.title);
              } else {
                const dataErr = JSON.parse(e.error);
                this.presentAccept(dataErr.response.responseMsg);
              }
            });
        }
      }
    }
  }

  selectChanged(selectedValue) {
    console.log('selector: ', selectedValue);
  }

  getPrefixs() {
    this.apiService.GetPrefixs().subscribe(
      (response) => {
        this.prefixsList = response.data;
        response.data.forEach((value, key) => {
          this.prefixsDesc[value.Prefix] = value.Prefix_desc;
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  async getProvince() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.getProvince().subscribe(
      (response) => {
        const listData = JSON.parse(response);
        this.provList = listData.data;
        // console.log(this.provList);
        listData.data.forEach((value, key) => {
          this.provCodeGuid[value.Pro_id] = value;
        });
        loading.dismiss();
        // console.log(this.provCodeGuid);
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async getDistrict(provId) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.getDistrict(provId).subscribe(
      (response) => {
        const listData = JSON.parse(response);
        this.distList = listData.data;
        listData.data.forEach((value, key) => {
          this.ampCodeGuid[value.Dist_id] = value;
        });
        loading.dismiss();
        console.log(this.ampCodeGuid);
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async getSubDistrict(distId) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.getSubDistrict(distId).subscribe(
      (response) => {
        const listData = JSON.parse(response);
        this.subdistList = listData.data;
        listData.data.forEach((value, key) => {
          this.distCodeGuid[value.Sub_id] = value;
        });
        console.log(this.subdistList);
        loading.dismiss();
        // console.log(this.distCodeGuid);
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  selectSubDistrict(subDistId) {
    this.currentPosition = { lat: this.distCodeGuid[subDistId].Sub_latitude, lng: this.distCodeGuid[subDistId].Sub_longitude };
    this.currentMarker.setPosition(this.currentPosition);
    this.map.panTo(this.currentPosition);
    this.map.setZoom(13);
  }

  termCondition() {
    this.nav.navigateForward('/termcondition');
  }

  async presentAlertConfirm(MaxCard) {
    let dialogMsg = 'คุณทำการสมัครสมาชิกเรียบร้อย';
    if (this.registerType === '00') {
      dialogMsg = 'ท่านได้รับ 100 แต้ม สามารถใช้สิทธิ์แลกแต้ม รับส่วนลดในวันถัดไป MaxCard ID : ' + MaxCard;
    }

    const alert = await this.alertCtrl.create({
      header: 'การสมัคร',
      message: dialogMsg,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {
            this.router.navigateByUrl('/home');
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  async registerLogin() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiService.UsersLogin(this.registerForm.value).subscribe(
      (response) => {
        console.log(response);
        let title = '';
        let message = '';
        let state = 0;
        if (response === undefined) {
          title = 'แจ้งเตือน';
          message = 'ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบ Username / Password';
          state = 0;
          this.presentAlertLogin(title, message, state);
          loading.dismiss();
        } else {
          loading.dismiss();
          this.storageService.setObject('logined', response);
          this.router.navigateByUrl('/tabs');
        }
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async presentAccept(text) {
    const alert = await this.alertCtrl.create({
      header: 'Warning',
      message: text,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'OK',
          handler: () => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  async presentExits() {
    const alert = await this.alertCtrl.create({
      header: 'Warning',
      message: 'This username Already in use Please enter a new username.',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.registerForm.controls.username.setValue('');
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  async checkUsername() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    const user = {
      user: this.registerForm.value.username,
    };
    this.apiService.checkusernameexits(user).subscribe((response) => {
      if (response.status !== '0') {
        this.presentExits();
        loading.dismiss();
      }
      loading.dismiss();
    });
  }

  async checkEmail() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    const user = {
      email: this.registerForm.value.email,
    };
    this.apiService.checkemailexits(user).subscribe((response) => {
      if (response.status !== '0') {
        this.presentEmailExits();
        loading.dismiss();
      }
      loading.dismiss();
    });
  }

  async presentEmailExits() {
    const alert = await this.alertCtrl.create({
      header: 'Warning',
      message: 'This Email Already in use Please enter a new Email.',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.registerForm.controls.email.setValue('');
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  public openModal() {
    this.router.navigateByUrl('/pdpa');
  }

  async setZipcode(zipcode) {
    alert(zipcode);
    this.zipcode = zipcode;
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
}
